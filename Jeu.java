import java.util.*;

/**
 * Classe principale du jeu "Zork".
 * <p>
 *
 * Zork est un jeu d'aventure tr�s rudimentaire avec une interface en mode
 * texte.
 *
 * Cette classe est une intersection de toutes les autres classes du jeu "Zork".
 * Toutes les actions, toutes les commandes du jeu commencent � s'executer ici.
 * Cette classe initialise toutes les pieces touts les objets et touts les �tres
 * qui se trouvent dans ces pieces. Cette classe est charg� de donner toutes les
 * �tapes d'execution des commandes.
 * 
 * Les regles de jeu sont simples. Le but est de trouver 5 cl�s qui sont les
 * cl�s de la sortie. Cette sortie se trouve au nord de la piece qui a une
 * description "sortie nord". Il suffit de porter toutes les cl�s et de tapper
 * commande 'aller nord' dans la piece "sortie nord" pour gagner. Les cl�s sont
 * reparties dans les differentes pieces. Elles peuvent se cacher dans des
 * objets compos�s. Pour prendre les cl�s ou bien n'importe quel autre objet
 * transportable il suffit de tapper commande 'prendre' suivi du code d'objet �
 * prendre. Pour acheter un objet payant il faut utiliser commande 'acheter'
 * suivi du code d'objet � acheter.Pour jeter un objet Zork qui est actuelement
 * transport� par joueur, il suffit de tapper la commande 'jeter' suivi du code
 * d'objet � jeter. Si le joueur veut jeter tous les objets port�, il peut taper
 * la commande 'jeterTous'. Pour se deplacer dans une autre piece l'utilisateur
 * doit tapper 'aller' et donner une direction (nord, sud, est, ouest). Il
 * existe m�me la commande qui permet de revenir dans la piece pr�c�dement
 * visit�, cette commande est 'retour'. Pour savoir quelles objets joueur porte
 * actuellement il existe la commende 'objets', et pour savoir dans quelles
 * piece le joueur se trouve actuellement et quelles sont des objets et des
 * sorties de cette piece il suffit de tapper la commande 'jeterOeil'. La
 * commande 'consulter' suivi du code d'un objet � consulter permet de savoir
 * quelles sont les objets Zork qui se trouvent dans un objet compos�, permet de
 * savoir quelle est le prix d'un objet payant et les autres particularit�s. Il
 * est recommand� d'utiliser d'abord cette commande avant d'utiliser la commande
 * 'prendre' ou 'acheter'. Chaque fois quand joueur se deplace, il existe une
 * possibilit� de recontrer des �tres (fantome ou demon). Dans cette version de
 * jeu il y a que 1 fantome et 1 demon. Il se deplace aleotoirement, donc le
 * joueur ne peut pas savoir o� ils se trouvent. Si le joueur rencontre �tre, il
 * doit repondre � sa question. Si la reponse est fautive l'�tre tue le joueur,
 * et le jeu est perdu, sinon il lui laisse partir. Les questions ne se repetent
 * pas. Pour repondre � question l'utilisateur doit taper la commande 'repondre'
 * suivi du nom d'�tre suivi du reponse ("a", "b", "c", "d"). Le joueur peut
 * rencontrer plusieurs �tre � la fois, dans ce cas ils vont pos� des question
 * un apr�s l'autre. Si l'�tre a d�ja pos� tous les questions, alors l'�tre
 * laisse joueur partir. Les �tres se deplace quand le joueur se deplace d'une
 * piece � l'autre. Si l'utilisateur souhaite quitter le jeu il peut tapper la
 * commande 'quitter', et le jeu s'arrete, mais c'est une parmi des situations
 * perdants. Et enfin pour consulter les effets des commandes et leur syntaxe il
 * suffit de tapper 'aide'.
 * 
 * Chaque instance de cette classe possede un attribut qui est analyseur
 * syntaxique. Cet attribut permet de verifier la validit� des commandes tapp�s
 * par l'utilisteur. L'action de jeu se deroule � un moment donn� dans une seul
 * piece. L'attribut qui s'appele la "pieceCourante" contient reference vers la
 * piece o� se trouve actuellement joueur. L'utilisateur peut manipuler que des
 * objets et les sorties de la piece courante. La carte de toutes les piece
 * represente la carte de l'universit� Paris 13. Il existe m�me la piece de
 * t�l�portation. Quand le joueur entre dans cette piece il se teleporte dans
 * une piece aleotoire. Un jeu contient un seul joueur. L'utilisateur donne les
 * commande a ce joueur, comme par exemple 'jeter', 'prendre', 'acheter' etc.
 * Pour pouvoir executer la commande 'retour' on a d� de ajouter un autre
 * attribut qui s'appele "directionPrecedent". Cet attribut sauvegarde la
 * direction de la sortie pr�c�dent qu'on a pass�. Ca nous permet de savoir de
 * quelle porte on a entr� dans cette piece. On peut pas utiliser cette commande
 * au debut de jeu et juste apres la teleportation. Et en fin le jeu possede
 * boolean "chienParti" qui est "false" au debut. Il existe une piece qui a
 * comme description une chaine "Bibliotheque. Attention, le chien agressive. Il
 * faut le nourir". Cette piece est proteg� par un chien. L'utilisateur ne peut
 * pas entrer dans cette piece s'il ne porte pas la viande. Si il tente de
 * passer, le deuxieme situation perdant d'arrive. Le chien tue le joueur, et le
 * jeu s'arrete. Mais si le joueur entre dans cette piece avec la viande, le
 * chien prend cette tranche de la viande et parti, la description de cette
 * piece se change vers "Bibliotheque" et le boolean "chienParti" devient "vrai"
 * et restera "vrai" jusqu'a la fin du jeu. Apres �a le joueur peut entrer et
 * sortir de cette piece librement. Le jeu possede la liste des �tres, pour
 * poivir sauvegarder et manipuler tous les �tres dans jeu. La liste des �tres
 * est implement� grace � ArrayList<Etre>. Pour cette version du jeu on a que 2
 * �tre dans le jeu (1 fantome et 1 demon).
 * 
 * La classe possede les methodes qui permettent d'organiser l'execution des
 * commandes, les methodes qui permettent d'initialiser le jeu (c'est � dire
 * cr�er et initialiser les pieces avec les objets dedans, cr�er et initialiser
 * l'analyseur syntaxique et creer le joueur).
 * 
 * Le methode qui permet de demarer et de jouer le jeu s'appel "jouer". Ce
 * methode s'execute jusqu'au l'evenment qui signalisera la fin du jeu (comme
 * par exemple la commande 'quitter').
 *
 * @author Michael Kolling
 * @author Marc Champesme (pour la traduction francaise)
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2017
 * @since Decembre 2017
 * 
 * @invariant getAnalyseurSyntaxique()!=null
 * @invariant getPieceCourante()!=null
 * @invariant getEtres()!=null
 * @invariant getEtres().size() == 2
 * @invariant getEtres().get(0) instanceof Fantome
 * @invariant getEtres().get(1) instanceof Demon
 * @ensures getEtres().get(0).getNom().equals("Casper")
 * @ensures getEtres().get(1).getNom().equals("Abbadon")
 */
public class Jeu {
	/**
	 * Le jeu possede un analyseur syntaxique (l'instance de la classe
	 * AnalyseurSyntaxique), qui permet de verifier la validit� des commandes tapp�s
	 * par l'utilisteur.
	 */
	private AnalyseurSyntaxique analyseurSyntaxique;
	/**
	 * L'instance de la classe Piece, qui correspond � la piece o� se trouve
	 * actuelement joueur. L'action de jeu se deroule � un moment donn� dans une
	 * seul piece (qui est la piece courante).
	 */
	private Piece pieceCourante;
	/**
	 * Un jeu contient un seul joueur (l'instance de la classe Joueur).
	 * L'utilisateur donne les commande a ce joueur, comme par exemple 'jeter',
	 * 'prendre', 'acheter' etc.
	 *
	 */
	private Joueur joueur;
	/**
	 * Chaine de caracteres, qui correspend � la direction de la sortie pr�c�dent
	 * qu'on a pass� (par exemple, si on a entr� dans la pieceCouranre en utilisant
	 * la commande 'aller sud' alors la direction pr�c�dent correspond � "sud".
	 */
	private String directionPrecedente;
	/**
	 * Boolean qui permet de savoir est-ce que le chien dans la piece dont
	 * description est "Bibliotheque. Attention, le chien agressive. Il faut le
	 * nourir" est parti ou non.
	 * 
	 */
	private boolean chienParti;

	/**
	 * Le jeu contient un certain nombre d'�tres. Ces �tres sont stock�s dans une
	 * ArrayList qui contient tous les �tres dans jeu.
	 */
	private ArrayList<Etre> etres;

	/**
	 * Constructeur, qui cr�e le jeu, initialise l'analyseur syntaxique et
	 * initialise la carte du jeu (i.e. les pi�ces avec les objets Zork qu'ils
	 * possedent et les �tres). Le jeu commencera dans la piece dont description est
	 * "La sortie nord". Ce constructeur fait appel au methode auxilliaire
	 * "creerPieces" qui se charge de cr�er des pieces avec des objets dedans et de
	 * creer tous les �tres et inisialiser leur attributs (nom, maitrise, piece
	 * initiale, liste des questions).
	 * 
	 * @ensures getAnalyseurSyntaxique()!=null
	 * @ensures getPieceCourante().getDescription().equals("La sorite nord")
	 * @ensures getJoueur()==null
	 * @ensures getDirectionPrecedent()==null
	 * @ensures !chienEstParti()
	 * @ensures getEtres()!=null
	 * @ensures getEtres().size() == 2
	 * @ensures getEtres().get(0) instanceof Fantome
	 * @ensures getEtres().get(1) instanceof Demon
	 * @ensures getEtres().get(0).getNom().equals("Casper")
	 * @ensures getEtres().get(1).getNom().equals("Abbadon")
	 * @ensures getEtres().get(0).getPieceCourante().getDescription().equals("Dehors")
	 * @ensures getEtres().get(0).getPieceCourante().getDescription().equals("Forum")
	 */
	public Jeu() {
		creerPieces();
		analyseurSyntaxique = new AnalyseurSyntaxique();
	}

	/**
	 * Methode, qui cr�e toutes les pieces et initialise leurs attributs, qui cr�e
	 * toutes les objets dans ces pieces et initialise leurs attributs et qui creer
	 * tous les �tres et initialise leur attributs (nom, maitrise, piece initiale,
	 * liste des questions). En plus le methode relie leurs sorties les unes aux
	 * autres. Ce methode fait appele aux methodes auxilliaires qui se chargent de
	 * cr�er des objets dans ces pieces.
	 * 
	 * @ensures getPieceCourante().getDescription().equals("La sorite nord")
	 * @ensures getPieceCourante().contient("cle1")
	 * @ensures getPieceCourante().getSorties().get("nord")==null
	 * @ensures getPieceCourante().getSorties().get("est").getDescription().equals("DAPS")
	 * @ensures getPieceCourante().getSorties().get("sud").getDescription().equals("Dehors")
	 * @ensures getPieceCourante().getSorties().get("ouest").getDescription().equals("Le
	 *          batiment C")
	 * @ensures getEtres()!=null
	 * @ensures getEtres().size() == 2
	 * @ensures getEtres().get(0) instanceof Fantome
	 * @ensures getEtres().get(1) instanceof Demon
	 * @ensures getEtres().get(0).getNom().equals("Casper")
	 * @ensures getEtres().get(1).getNom().equals("Abbadon")
	 * @ensures getEtres().get(0).getPieceCourante().getDescription().equals("Dehors")
	 * @ensures getEtres().get(1).getPieceCourante().getDescription().equals("Forum")
	 */
	private void creerPieces() {
		// Declaration des variables locaux
		Piece teleport;
		Piece daps;
		Piece sortieNord;
		Piece batimentC;
		Piece amphisABCD;
		Piece dehors;
		Piece institutGalilee;
		Piece bibliotheque;
		Piece forum;
		Piece accueil;
		Piece sortieSud;
		Piece restoU;

		// cr�ation des pieces avec des objets dedans
		teleport = new Piece("La piece de teleportation");
		daps = new Piece("DAPS", creerObjetsDaps());
		sortieNord = new Piece("La sortie nord", creerObjetsSortieNord());
		batimentC = new Piece("Le batiment C", creerObjetsBatimentC());
		amphisABCD = new Piece("Les Amphis A, B, C et D", creerObjetsAmphisABCD());
		dehors = new Piece("Dehors", creerObjetsDehors());
		institutGalilee = new Piece("Institut Galilee", creerObjetsInstitutGalilee());
		bibliotheque = new Piece("Bibliotheque. Attention, le chien agressive. Il faut le nourir",
				creerObjetsBibliotheque());
		forum = new Piece("Forum", creerObjetsForum());
		accueil = new Piece("Accueil", creerObjetsAccueil());
		sortieSud = new Piece("La sortie sud");
		restoU = new Piece("RestoU", creerObjetsRestoU());

		// initialise les sorties des pieces
		teleport.setSorties(null, null, null, daps);
		daps.setSorties(null, teleport, amphisABCD, sortieNord);
		sortieNord.setSorties(null, daps, dehors, batimentC);
		batimentC.setSorties(null, sortieNord, institutGalilee, null);
		amphisABCD.setSorties(daps, null, bibliotheque, dehors);
		dehors.setSorties(sortieNord, amphisABCD, forum, institutGalilee);
		institutGalilee.setSorties(batimentC, dehors, accueil, null);
		bibliotheque.setSorties(amphisABCD, null, null, forum);
		forum.setSorties(dehors, bibliotheque, sortieSud, accueil);
		sortieSud.setSorties(forum, null, restoU, null);
		accueil.setSorties(institutGalilee, forum, null, null);
		restoU.setSorties(sortieSud, null, null, null);

		// creation et initialisation des �tres
		etres = new ArrayList<Etre>(2);
		Fantome fantome = new Fantome("Casper", dehors);
		Demon demon = new Demon("Abbadon", forum);
		etres.add(fantome);
		etres.add(demon);

		// le jeu commence dans la piece dont description est "La sortie nord"
		pieceCourante = sortieNord;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "DAPS".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "DAPS".
	 * 
	 * @ensures \result != null
	 * @ensures \result.size() == 2
	 */
	private ArrayList<ObjetZork> creerObjetsDaps() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork table = new ObjetZork("tableau", "Le tableau du bois", 8, false);
		ObjetZork ballon = new ObjetZork("ballon", "Le ballon du football", 1, true);
		list.add(table);
		list.add(ballon);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "La sortie nord".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "La sortie nord".
	 * 
	 * @ensures \result != null
	 * @ensures \result.size() == 1
	 */
	private ArrayList<ObjetZork> creerObjetsSortieNord() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		Cle cle = new Cle("cle1", "Cle de la sortie");
		list.add(cle);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "Le batiment C".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "Le batiment C".
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() == 3
	 */
	private ArrayList<ObjetZork> creerObjetsBatimentC() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork tableau = new ObjetZork("tableau", "Le tableau du verre", 6, false);
		ObjetZork emploiDuTemps = new ObjetZork("EmploiDuTemps", "Board du emploi du temps", 3, false);
		ObjetZork livre = new ObjetZork("livreMath", "Livre de mathematique", 1, true);
		list.add(tableau);
		list.add(emploiDuTemps);
		list.add(livre);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "Les Amphis A, B, C et
	 * D".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "Les Amphis A, B, C et D".
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() == 3
	 */
	private ArrayList<ObjetZork> creerObjetsAmphisABCD() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork cahier = new ObjetZork("cahier", "Cahier physique", 1, true);
		ObjetZork stylo = new ObjetZork("stylo", "Stylo bleue", 0, true);
		ObjetZork monnaire = new ObjetZork("1euro", "Une monnaire d'un euro", 0, true);
		list.add(cahier);
		list.add(stylo);
		list.add(monnaire);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "Dehors".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "Dehors".
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() == 1
	 */
	private ArrayList<ObjetZork> creerObjetsDehors() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork pierre = new ObjetZork("pierre", "Un pierre", 1, true);
		list.add(pierre);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "Institut Galilee".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "Institut Galilee".
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() == 3
	 */
	private ArrayList<ObjetZork> creerObjetsInstitutGalilee() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork microonde = new ObjetZork("microonde", "Une microonde", 5, false);
		ObjetZork livre = new ObjetZork("livreAnalyse", "Livre de analyse", 1, true);
		ObjetCompose tiroir = new ObjetCompose("tiroir", "Une tiroir", 5, false);
		Cle cle = new Cle("cle2", "Cle de la sortie");
		tiroir.ajouter(cle);
		list.add(microonde);
		list.add(livre);
		list.add(tiroir);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "Bibliotheque.
	 * Attention, le chien agressive. Il faut le nourir".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "Bibliotheque. Attention, le chien agressive. Il faut
	 *         le nourir".
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() == 5
	 */
	private ArrayList<ObjetZork> creerObjetsBibliotheque() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork livre1 = new ObjetZork("livreHistoire", "Le livre d'histoire", 1, true);
		ObjetZork livre2 = new ObjetZork("livreAlgorithmique", "Livre d'algorithmique", 1, true);
		ObjetCompose livre3 = new ObjetCompose("livrePOO", "Livre POO", 1, true);
		Cle cle = new Cle("cle3", "Cle de la sortie");
		livre3.ajouter(cle);
		ObjetZork livre4 = new ObjetZork("livreBiologie", "Livre de bilogie", 1, true);
		ObjetZork livre5 = new ObjetZork("livreEconomique", "Livre d'economique", 1, true);
		list.add(livre1);
		list.add(livre2);
		list.add(livre3);
		list.add(livre4);
		list.add(livre5);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "Forum".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "Forum".
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() == 4
	 */
	private ArrayList<ObjetZork> creerObjetsForum() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork table = new ObjetZork("tableau", "Le tableu du bois", 8, false);
		ObjetCompose chaise1 = new ObjetCompose("chaise1", "La chaise", 3, false);
		ObjetZork chaise2 = new ObjetZork("chaise2", "La chaise", 3, false);
		ObjetZork chaise3 = new ObjetZork("chaise3", "La chaise", 3, false);
		Cle cle = new Cle("petitCle", "Petit cle");
		chaise1.ajouter(cle);
		list.add(table);
		list.add(chaise1);
		list.add(chaise2);
		list.add(chaise3);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "Accueil".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "Accuiel".
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() == 2
	 */
	private ArrayList<ObjetZork> creerObjetsAccueil() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork carteEtudiante = new ObjetZork("carteEtudiante", "La carte Etudiante", 0, true);
		ObjetCompose placard = new ObjetCompose("placard", "Le placard du bois", 100, false);
		ObjetCompose coffreFort = new ObjetCompose("coffreFort", "Le coffre-fort", 20, false);
		Cle cle = new Cle("cle4", "Cle de la sortie");
		coffreFort.ajouter(cle);
		placard.ajouter(coffreFort);
		list.add(carteEtudiante);
		list.add(placard);
		return list;
	}

	/**
	 * Methode auxilliaire qui permet de cr�er et retourner la liste des objets Zork
	 * qui sera affecter � la piece dont la description est "RestoU".
	 * 
	 * @return Instance de la classe ArrayList<ObjetZork>, qui correspond � liste
	 *         des objets dej� initialis�s qui sera affecter � la piece dont la
	 *         description est "RestoU".
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() == 3
	 */
	private ArrayList<ObjetZork> creerObjetsRestoU() {
		ArrayList<ObjetZork> list = new ArrayList<ObjetZork>();
		ObjetZork monnaire = new ObjetZork("1euro", "Une monnaire d'un euro", 0, true);
		ObjetPayant viande = new ObjetPayant("viande", "Le viande du moutnons", 3, 2);
		Cle cle = new Cle("cle5", "Cle de la sortie");
		list.add(monnaire);
		list.add(viande);
		list.add(cle);
		return list;
	}

	/**
	 * Methodes qui permet de cr�er et initialiser le joueur, qui permet demarer et
	 * de jouer le jeu. Ce methode fait appel au methode auxilliaire qui permet
	 * d'afficher la message de bienvenue et de creer le joueur et initialiser ses
	 * attributs en posant quelques questions au utilisateur. Ce methode contient
	 * boucle 'while' qui s'execute tant que un evenement qui signalise la fin du
	 * jeu n'arrive pas (comme par exemple la commande 'quitter').
	 * 
	 * @ensures getJoueur != null
	 */
	public void jouer() {
		// Appel au methode auxiliare.
		afficherMsgBienvennue();

		/*
		 * Entr�e dans la boucle principale du jeu. Cette boucle lit et ex�cute les
		 * commandes entr�es par l'utilisateur, jusqu'a ce que la commande choisie m�ne
		 * � la fin du jeu
		 */
		boolean termine = false;
		while (!termine) {
			Commande commande = analyseurSyntaxique.getCommande();
			termine = traiterCommande(commande);
		}
		System.out.println("Merci d'avoir jouer.  Au revoir.");
	}

	/**
	 * Methode auxilliaire qui permet d'afficher la message de bienvenue (qui
	 * contient l'explication sur ce jeu) et cr�e le joueur et initialise ses
	 * attributs en posant quelques questions au utilisateur. Ce methode appele le
	 * methode auxiliaire "demanderQuestions". Ce methode est appel� par le methode
	 * "jouer".
	 * 
	 * @ensures getJoueur() != null
	 */
	private void afficherMsgBienvennue() {
		System.out.println();
		System.out.println("Bienvennue dans le monde de Zork !");
		System.out.println(
				"Le but de ce jeu est de trouver toutes les 5 cl�s et de sortir d'universit�, qui est actuellemnet ferm�e.");
		System.out.println(
				"Il y a deux maniere de sortir. Si vous trouverez toutes les cl� et vous sortirez au nord de la 'La sortie nord' - vous etes gagneur");
		System.out.println("Attention, vous pouvez rencontrez des etres. Faites anntention, si vouz lui donnerez la reponse fautive il vous tuera");
		System.out.println(
				"Mais si vous abbandonerez le jeu en tapant la commande 'quitter', ou bien si vous mourrez - vous etes perdu");
		System.out.println(
				"Vous pouvez consulter toutes les commandes disponible, leurs effets et leurs syntaxe - en tapant 'aide'.");
		
		demanderQuestions();
		System.out.println("Bon courage � vous!");
		System.out.println();
		System.out.println(pieceCourante.descriptionComplet());
	}

	/**
	 * Methode auxilliaire qui cr�e le joueur et initialise ses attributs en posant
	 * quelques questions au utilisateur. Ce methode est appl� par le methode
	 * "afficherMsgBienvennue".
	 * 
	 * @ensures getJoueur() != null
	 * @ensures getJoueur().getPoidsMax() == 3 || getJoueur().getPoidsMax() == 6
	 */
	private void demanderQuestions() {
		boolean homme;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"Avant de commencer, on est obliger de connaitre le nom de notre ch�r(e) joueur et votre sexe");
		System.out.println("Votre nom : ");
		String nom = sc.nextLine();
		System.out.println("Votre sexe(true si homme, false sinon) : ");
		try {
			homme = sc.nextBoolean();
		} catch (InputMismatchException e) {
			System.out.println("Vous etes pas serieux! Alors on va supposer que vous etes une fille");
			homme = false;
		}
		joueur = (homme) ? new Joueur(nom, 6) : new Joueur(nom, 3);
	}

	/**
	 * Methode qui retourne l'analyseur syntaxique de ce jeu.
	 * 
	 * @return L'instance de la classe AnalyseurSyntaxique, qui correspend �
	 *         l'analyseur syntaxique de ce jeu.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public AnalyseurSyntaxique getAnalyseurSyntaxique() {
		return analyseurSyntaxique;
	}

	/**
	 * Methode qui permet de savoir quelle est la piece courante de ce jeu.
	 * 
	 * @return L'instance de la classe Piece, qui correspend � la piece courante de
	 *         ce jeu.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public Piece getPieceCourante() {
		return pieceCourante;
	}

	/**
	 * Methode qui permet de savoir quel est le joueur courante de ce jeu.
	 * 
	 * @return L'instance de la classe Joueur, qui correspend au joueur de ce jeu.
	 * 
	 * @pure
	 */
	public Joueur getJoueur() {
		return joueur;
	}

	/**
	 * Methode qui permet de savoir quelle est la direction de la sortie pr�c�dent
	 * qu'on a pass�.
	 * 
	 * @return Chaine de caracteres, qui correspend � la direction de la sortie
	 *         pr�c�dent qu'on a pass�.
	 * 
	 * @pure
	 */
	public String getDirectionPrecedent() {
		return directionPrecedente;
	}

	/**
	 * Methode qui permet de savoir est-ce que le chien dans la piece dont
	 * description est "Bibliotheque. Attention, le chien agressive. Il faut le
	 * nourir" est parti ou non.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i> si le chien est parti <br>
	 *         <i>false</i> sinon.
	 * 
	 * @pure
	 */
	public boolean chienEstParti() {
		return chienParti;
	}

	/**
	 * Methode, qui retourne la liste de tous les �tres existants dans le jeu.
	 * 
	 * @return L'instance du type ArrayList<Etre>, qui represente la liste des �tres
	 *         existants dans le jeu.
	 * 
	 * @ensures \result.size() == 2
	 * @ensures \result.get(0) instanceof Fantome
	 * @ensures \result.get(1) instanceof Demon
	 * @ensures \result.get(0).getNom().equals("Casper")
	 * @ensures \result.get(1).getNom().equals("Abbadon")
	 * @pure
	 */
	public ArrayList<Etre> getEtres() {
		return etres;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux jeux.
	 * 
	 * @param o
	 *            Instance de la class Object, avec laquelle on va comparer notre
	 *            jeu.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux jeux ont le contenu identiques <br>
	 *         <i>false</i>, si les deux jeux ont les contenus differentes ou si "o"
	 *         n'est pas une instance de la classe jeu.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof Jeu) ==> !\result
	 * @ensures (o instanceof Jeu) ==> \result <==>
	 *          (this.getPieceCourante().equals(((Jeu) o).getPieceCourante()) &&
	 *          this.getJoueur().equals(((Jeu) o).getJoueur()) &&
	 *          this.chienEstParti() == ((Jeu) o).chienEstParti() &&
	 *          this.getDirectionPrecedent().equals(((Jeu)
	 *          o).getDirectionPrecedent()) && this.getEtres().equals(((Jeu)
	 *          o).getEtres()))
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("L'objet passe doit etre different de null");
		}
		if (!(o instanceof Jeu)) {
			return false;
		}
		Jeu j = (Jeu) o;
		return pieceCourante.equals(j.pieceCourante) && joueur.equals(j.joueur)
				&& directionPrecedente.equals(j.directionPrecedente) && chienParti == j.chienParti
				&& etres.equals(j.etres);
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * jeux sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de ce jeu.
	 * 
	 * @ensures (\forall Jeu j1, j2 ; j1.equals(j2) ; j1.hashCode() ==
	 *          j2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		int hc = pieceCourante.hashCode() + joueur.hashCode() + directionPrecedente.hashCode() + etres.hashCode();
		hc *= (chienParti) ? 1 : -1;
		return hc;
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de ce jeu, tel que
	 * si et seulement si deux jeux sont �gaux alors ils ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente ce jeu.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall Jeu j1, j2 ; j1.equals(j2) ;
	 *          j1.toString().equals(j2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String str = "Jeu piece = (" + pieceCourante.toString() + ") joueur = (" + joueur.toString()
				+ ") directionPrecedent = (" + directionPrecedente + ") ";
		str += (chienParti) ? "chien parti " : "non chien parti ";
		str += "etres : " + etres.toString();
		return str;
	}

	/**
	 * Methode, qui ex�cute la commande sp�cifi�e. Si cette commande termine le jeu,
	 * alors methode renvoie true , sinon il renvoie false. Ce methode fait appele
	 * aux plusieur methodes auxiliaire qui permettent d'executer les commandes
	 * differentes. Methode renvoie true dans quatre cas : si le mot-cl� de la
	 * commande correspond � 'quitter', ou si le joueur a donn� la reponse fautive �
	 * �tre, ou si le joueur tente d'entrer dans la piece dont la description est
	 * "Bibliotheque. Attention, le chien agressive. Il faut le nourir" et le joueur
	 * ne possede pas la viande, ou si la jouer tappe la commande 'aller nord'
	 * tanque il est dans la piece dont description est "La sortie nord" et il a
	 * toutes les cl�s de cette sortie.
	 *
	 * @param commande
	 *            L'instance de la Classe commande, qui corespond � la commande
	 *            qu'on doit ex�cuter.
	 * @return Boolean, qui est :<br>
	 *         <i>true</i> si cette commande termine le jeu <br>
	 *         <i>false</i> sinon.
	 * 
	 * @requires commande!=null
	 * @ensures ((commande.getMotCommande().equals("quitter") &&
	 *          !commande.aSecondMot()) ||
	 *          ((commande.getMotCommande().equals("aller") &&
	 *          commande.getSecondMot().equals("est") &&
	 *          getPieceCourante().getDescription().equals("Forum") &&
	 *          !chienEstPartie()) || commande.getMotCommande().equals("aller") &&
	 *          commande.getSecondMot().equals("sud") &&
	 *          getPieceCourante().getDescription().equals("Les Amphis A, B, C et
	 *          D") && !chienEstPartie()) ||
	 *          (commande.getMotCommande().equals("aller") &&
	 *          commande.getSecondMot().equals("nord") &&
	 *          getPieceCourante().getDescription().equals("La sortie nord") &&
	 *          getJoueur().contientCombienDeCle() == 5)) ==> \result
	 * @throws NullPointerException
	 *             si la commande pass� est null
	 */
	public boolean traiterCommande(Commande commande) {
		if (commande == null) {
			throw new NullPointerException("La commande passe doit etre different de null.");
		}
		if (commande.isInconnue()) // si la commane entr� n'est pas valide
		{
			System.out.println("Je ne comprends pas ce que vous voulez...");
			return false;
		}

		String motCommande = commande.getMotCommande();
		if (motCommande.equals("aide")) // l'execution de la commande 'aide'
		{
			afficherAide();
		} else if (motCommande.equals("aller")) // l'execution de la commande 'aller'
		{
			return deplacerVersAutrePiece(commande);
		} else if (motCommande.equals("prendre")) // l'execution de la commande 'prendre'
		{
			prendreObjet(commande);
		} else if (motCommande.equals("jeter")) // l'execution de la commande 'jeter'
		{
			jeterObjet(commande);
		} else if (motCommande.equals("consulter")) // l'execution de la commande 'consulter'
		{
			consulterObjet(commande);
		} else if (motCommande.equals("acheter")) // l'execution de la commande 'acheter'
		{
			acheterObjet(commande);
		} else if (motCommande.equals("retour")) // l'execution de la commande 'retour'
		{
			retourDansPiecePrecedent();
		} else if (motCommande.equals("objets")) // l'execution de la commande 'objets'
		{
			afficherObjetsDeJoueur();
		} else if (motCommande.equals("quitter")) // l'execution de la commande 'quitter'
		{
			return quitterJeu(commande);
		} else if (motCommande.equals("jeterOeil")) // l'execution de la commande 'jeterOeil'
		{
			if (commande.aSecondMot()) {
				System.out.println("La commande 'jeterOeil' s'ecrit sans deuxiem mot");
			} else {
				System.out.println(pieceCourante.descriptionComplet());
			}
		} else if (motCommande.equals("jeterTous")) {
			jeterTousObjets();
		} else if (motCommande.equals("repondre")) {
			System.out.println(
					"Vous ne pouvez pas donner votre reponse maintenant. Faite le quand l'�tre vous possera la question.");
		}
		return false;
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'aide'. Affiche la
	 * syntaxe de chaque commande et decrit toutes les effets de cette commande. Ce
	 * methode est appl� par methode "traiterCommande".
	 */
	private void afficherAide() {
		System.out.println(
				"Pour se deplecer d'une piece a une autre tappez le mot cl� 'aller' suivi d'une direction ('nord','ouest','est','sud')");
		System.out.println("Pour quitter le jeu tappez le mot cl� 'quitter'.");
		System.out.println("Pour prendre un objet transportable tappez 'prendre' suivi du code d'objet");
		System.out.println("Pour jeter un objet dans piece courante tappez 'jeter' suivi du code d'objet");
		System.out.println("Pour acheter un objet payant tappez 'acheter' suivi du code d'objet");
		System.out.println(
				"Pour voir les particularit�s d'un objet, pour savoir le prix d'un objet payant ou pour conna�tre que ce que il y � l'interieur d'un objet compos� tappez 'consulter' suivi du code d'objet");
		System.out.println("Pour voir toutes les objets qui sont transport�s par joueur actuellement tappez 'objets'");
		System.out.println("Pour jeter un oeil autour de vous tappez 'jeterOeil'");
		System.out.println("Pour revenir dans la piece pr�c�dement visit� tappez 'retour'");
		System.out.println("Pour jeter tous les objets que vous posseder dans une piece tappez 'jeterTous'");
		System.out.println(
				"Pour repondre a question d'�tre ecrivez la commande de cette maniere 'repondre nomEtre lettreReponse', ou la lettre de reponse est parmi a, b, c ou d.");
		System.out.println("Pour revoir cette message ult�rieurement tappez 'aide'");

		analyseurSyntaxique.afficherToutesLesCommandes();
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'aller'. Tente d'aller
	 * dans la direction sp�cifi�e par la commande. Si la piece courante poss�de une
	 * sortie dans cette direction, la piece correspondant � cette sortie devient la
	 * piece courante et la direction sera stocker dans l'attribut
	 * "directionPrecedent", dans les autres cas affiche un message d'erreur. Si la
	 * commande m�ne � la fin du jeu, alors methode retournera true, sinon methode
	 * retournera false. Ce methode appele le methode auxilliaire qui se charge de
	 * deplacer tous les �tres dans jeu, et si la piece d'un �tre sera egale � la
	 * piece de joueur alors il posera une question. Ce methode traite plusieurs cas
	 * particulaires qui peuvent s'arriver lors de deplacement. Ce methode est appl�
	 * par methode "traiterCommande".
	 * 
	 * @param commande
	 *            Instance de la classe Commande, dont le second mot soit sp�cifie
	 *            la direction � suivre, soit est n'importe quelle autre chaine,
	 *            soit est null.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i> si la commande m�ne � la fin du jeu.<br>
	 *         <i>false</i> sinon.
	 * 
	 * @requires commande!=null
	 * @requires commande.getMotCommande().equals("aller")
	 * @ensures (((commande.getMotCommande().equals("aller") &&
	 *          commande.getSecondMot().equals("est") &&
	 *          getPieceCourante().getDescription().equals("Forum") &&
	 *          !chienEstPartie()) || commande.getMotCommande().equals("aller") &&
	 *          commande.getSecondMot().equals("sud") &&
	 *          getPieceCourante().getDescription().equals("Les Amphis A, B, C et
	 *          D") && !chienEstPartie()) ||
	 *          (commande.getMotCommande().equals("aller") &&
	 *          commande.getSecondMot().equals("nord") &&
	 *          getPieceCourante().getDescription().equals("La sortie nord") &&
	 *          getJoueur().contientCombienDeCle() == 5)) ==> \result
	 * @throws NullPointerException
	 *             si la commande pass� est null
	 * @throws IllegalArgumentException
	 *             si le mot-cl� de la commande pass� est different de 'aller'
	 */
	private boolean deplacerVersAutrePiece(Commande commande) {
		if (commande == null) {
			throw new NullPointerException("La commande passe doit etre different de null.");
		}
		if (!commande.getMotCommande().equals("aller")) {
			throw new IllegalArgumentException("Le mot-cle de commande passe doit etre 'aller'");
		}
		if (!commande.aSecondMot()) // si la commande ne contient pas le deuxieme mot
		{
			System.out.println("Aller o� ?");
			return false;
		}

		String direction = commande.getSecondMot();
		// cas particulaires
		// cas 1
		if (pieceCourante.getDescription().equals("La sortie nord")
				&& direction.equals("nord")) /*
												 * si le joueur est dans la piece dont description est "La sortie nord"
												 * et il tente de aller au nord
												 */
		{
			if (joueur.contientCombienDeCle() == 5) /*
													 * si le joueur possedes toutes les cl�s de cette sortie, alors
													 * joueur a gagn�
													 */
			{
				System.out.println("Felicitations! Vous avez gagner!");
				return true;
			} else /* sinon la message d'erreur sera afficher */
			{
				System.out.println("Vous ne povez pas passer. Vous devez d'abord trouver toutes les cles");
				return false;
			}
		}
		// cas 2
		if (pieceCourante.getDescription().equals("La sortie sud") && direction.equals("sud") && !joueur.contient(
				"carteEtudiante")) /*
									 * si le joueur est dans la piece dont description est "La sortie sud" et il
									 * tente de aller au sud lorsque il ne possede pas objet Zork qui correspond �
									 * la carte etudiante, alors la message d'erreur sera afficher
									 */
		{
			System.out.println(
					"Vous ne povez pas partir � RestoU. Vous devez d'abord trouver la carte etudiante pour passer");
			return false;
		}
		// cas 3
		if (!chienParti) // si le chien n'est pas parti
		{
			if ((pieceCourante.getDescription().equals("Forum") && direction.equals("est"))
					|| (pieceCourante.getDescription().equals("Les Amphis A, B, C et D") && direction
							.equals("sud"))) /*
												 * si le joueur est dans la piece dont description est "Forum" et il
												 * tente de aller au l'est ou bien si le joueur est dans la piece dont
												 * description est "Les Amphis A, B, C et D" et il tente de aller au sud
												 */
			{
				if (!joueur.contient("viande")) /*
												 * si il ne possede pas objet Zork qui correspond � la viande, alors le
												 * chien lui tue et le jeu est terminer. Le joueur est perdant
												 */
				{
					System.out.println("Le chien vous a tu�. Vous etes perdu!");
					return true;
				} else /* sinon le chien prend cette tranche du viande et part */
				{
					joueur.jeter("viande");
					chienParti = true;
					System.out.println(
							"Vous avez donn� � chien votre tranche du viande, il est parti. Maintenant vous pouvez entrer dans la bibliotheque");
					if (pieceCourante.getDescription()
							.equals("Forum")) /*
												 * La description du piece dont l'ancien description �tait
												 * "Bibliotheque. Attention, le chien agressive. Il faut le
												 * nourir" change sa description pour "Bibliotheque"
												 */
					{
						pieceCourante.getSorties().get("est").setDescription("Bibliotheque");
					} else {
						pieceCourante.getSorties().get("sud").setDescription("Bibliotheque");
					}
				}
			}
		}

		// Tentative d'aller dans la direction indiqu�e.
		Piece pieceSuivante = pieceCourante.pieceSuivante(direction);
		if (pieceSuivante == null) // si la piece ne possede pas des sortie dans cette direction
		{
			System.out.println("Il n'y a pas de porte dans cette direction!	");
		} else // sinon
		{
			directionPrecedente = direction;
			pieceCourante = pieceSuivante;
			// si on a entr� dans une piece de teleportation
			if (pieceCourante.getDescription().equals("La piece de teleportation")) {
				teleportationDansAutrePiece();
				System.out.println("\nLa teleportation a reussi.");
			}
			/*
			 * Deplacement de tous les �tres. Si le joueur et l'�tre se trouvent dans une
			 * piece alors l'�tre pose lui question.
			 */
			boolean tueParEtre = deplacementEtTestEtres();
			if (!tueParEtre) {
				System.out.println(pieceCourante.descriptionComplet());
			}
			return tueParEtre;
		}
		return false;
	}

	/**
	 * Methode auxiliaire, qui permet � joueur de se teleport� dans une piece
	 * aleatoire. Ce methode doit �tre appel� uniquement si le joueur se trouve dans
	 * une piece de teleportation. Un entier est assosi� � chaque piece (de 0 � 10).
	 * L'entier est choisi �leotoirement gr�ce � instance de la classe Random. Apres
	 * la teleportaion, c'est impossible de revenir dans une piece precedent
	 * (commande 'retour'). Ce methode est appel� par methode
	 * "deplacerVersAutrePiece".
	 * 
	 * @requires getPieceCourante().getDescription().equals("La piece de
	 *           teleportation")
	 * @ensures !getPieceCourante().equals("La piece de teleportation")
	 * @ensures getDirectionPrecedent() == null
	 * @throws IllegalArgumentException
	 *             si la piece o� se trouve joueur n'est pas une piece de
	 *             teleportation.
	 */
	private void teleportationDansAutrePiece() {
		if (!pieceCourante.getDescription().equals("La piece de teleportation")) {
			throw new IllegalArgumentException("Vous devez se trouver dans une piece de teleportation");
		}
		// choix d'une entier aleatoir entre 0 et 11 (exclu).
		Random rand = new Random();
		int numeroPiece = rand.nextInt(11);

		// assosiation d'un nombre � chaque piece.
		switch (numeroPiece) {
		// piece "Daps"
		case 0:
			pieceCourante = pieceCourante.pieceSuivante("ouest");
			break;
		// piece "Sortie nord"
		case 1:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("ouest");
			break;
		// piece "Batiment C"
		case 2:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("ouest").pieceSuivante("ouest");
			break;
		// piece "Amphis A, B, C et D"
		case 3:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("sud");
			break;
		// piece "Dehors"
		case 4:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("sud").pieceSuivante("ouest");
			break;
		// piece "Institut Galilee"
		case 5:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("sud").pieceSuivante("ouest")
					.pieceSuivante("ouest");
			break;
		// piece "Bibliotheque"
		case 6:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("sud").pieceSuivante("sud");
			break;
		// piece "Forum"
		case 7:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("sud").pieceSuivante("sud")
					.pieceSuivante("ouest");
			break;
		// piece "Accueil"
		case 8:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("sud").pieceSuivante("sud")
					.pieceSuivante("ouest").pieceSuivante("ouest");
			break;
		// piece "Sortie sud"
		case 9:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("sud").pieceSuivante("sud")
					.pieceSuivante("ouest").pieceSuivante("sud");
			break;
		// piece "RestoU"
		default:
			pieceCourante = pieceCourante.pieceSuivante("ouest").pieceSuivante("sud").pieceSuivante("sud")
					.pieceSuivante("ouest").pieceSuivante("sud").pieceSuivante("sud");
		}
		directionPrecedente = null;
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'prendre'. Le joueur
	 * tente de prendre l'objet qui a le code correspondant au dexieme mot du
	 * commande specifi�. Si l'objet n'existe pas dans la piece, ou la commande ne
	 * possede pas le deuxieme mot, ou l'objet est payant et non-achet�, ou si
	 * l'objet n'est pas transportable, ou si le joueur ne peut plus prendre des
	 * objets alors le message d'erreur sera affich�. Sinon se objet sera retir� de
	 * la piece et ajout� � la liste des objets Zork port� par joueur. Ce methode
	 * est appl� par methode "traiterCommande".
	 * 
	 * @param commande
	 *            Istance de la classe Commande, dont le second mot soit sp�cifie le
	 *            code d'objet � prendre, soit est n'importe quelle autre chaine,
	 *            soit est null.
	 * 
	 * @requires commande != null
	 * @requires commande.getMotCommande().equals("prendre")
	 * @throws NullPointerException
	 *             si la commande pass� est null.
	 * @throws IllegalArgumentException
	 *             si le mot-cl� de la commande pass� est different de 'prendre'
	 */
	private void prendreObjet(Commande commande) {
		if (commande == null) {
			throw new NullPointerException("La commande passe doit etre different de null.");
		}
		if (!commande.getMotCommande().equals("prendre")) {
			throw new IllegalArgumentException("Le mot-cle de commande passe doit etre 'prendre'");
		}
		if (!commande.aSecondMot()) // si le deuxieme mot du commande est null alors message d'erreur est affich�
		{
			System.out.println("Prendre quoi?");
			return;
		}

		String codeObjet = commande.getSecondMot();
		if (!pieceCourante.contient(codeObjet)) /*
												 * si la piece ne possede pas objet du m�me code alors message d'erreur
												 * est affich�
												 */
		{
			System.out.println("L'objet " + codeObjet + " n'existe pas dans cette piece");
			return;
		} else {
			ObjetZork oz = pieceCourante.rechercherObjet(codeObjet); /*
																		 * oz contient reference vers ce objet
																		 */
			if ((oz instanceof ObjetPayant) && !((ObjetPayant) oz).isAchete()) // si ce objet est payant alors message
																				// d'erreur est affich�
			{
				System.out.println("Vouz pouvez pas prendre cette objet, vous devez l'acheter!");
				return;
			} else if (!oz.isTransportable()) // si objet n'est pas transportable alors message d'erreur est affich�
			{
				System.out.println("Vouz pouvez pas prendre cette objet, il n'est pas transportablbe!");
				return;
			} else if (!joueur.prendre(oz)) // si joueur ne peut plus transporter des objets alors message d'erreur est
											// affich�
			{
				return;
			} else // sinon il prend cette objet
			{
				pieceCourante.retirer(codeObjet); // enleve cet objet du piece courante
				if ((oz instanceof Cle) && (oz.getDescription().equals("Cle de la sortie"))) { // si cet objet est cl�
																								// alors message du
																								// felicitation est
																								// affich�
					String s = "Bravo ! Vous avez trouvez le cle! ";
					s += (5 - joueur.contientCombienDeCle() == 0)
							? " C'etait le dernier cle! Maintenant vous pouvez sortir"
							: "Il vous reste encore " + (5 - joueur.contientCombienDeCle()) + " cle a trover.";
					System.out.println(s);
				} else {
					System.out.println("L'objet " + codeObjet + " a ete prise");
				}
			}
		}
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'jeter'. Le joueur
	 * tente de jeter l'objet qui a le code correspondant au dexieme mot du commande
	 * specifi�. Si l'objet n'existe pas dans la liste des objets de joueur, ou la
	 * commande ne possede pas le deuxieme mot alors le message d'erreur sera
	 * affich�. Sinon se objet sera retir� de la liste des objets Zork port� par
	 * joueur et ajout� dans la piece courante. Ce methode est appl� par methode
	 * "traiterCommande".
	 * 
	 * @param commande
	 *            Istance de la classe Commande, dont le second mot soit sp�cifi� le
	 *            code d'objet � prendre, soit est n'importe quelle autre chaine,
	 *            soit est null.
	 * 
	 * @requires commande != null
	 * @requires commande.getMotCommande().equals("jeter")
	 * @ensures (\forall ObjetZork oz, String code; commande.getSecondMot() != null
	 *          && commande.getSecondMot().equals(code) && getJouer().contient(code)
	 *          && oz.equals(getJoueur().rechercherObjet(code));
	 *          (getJoueur().getObjets().size() ==
	 *          \old(getJoueur().getObjets()).size() - 1) &&
	 *          getPieceCournte().contient(code) &&
	 *          getPieceCournte().getObjets().size() ==
	 *          \old(getPieceCournte().getObjets()).size() +1)
	 * @throws NullPointerException
	 *             si la commande pass� est null.
	 * @throws IllegalArgumentException
	 *             si le mot-cl� de la commande pass� est different de 'jeter'
	 */
	private void jeterObjet(Commande commande) {
		if (commande == null) {
			throw new NullPointerException("La commande passe doit etre different de null.");
		}
		if (!commande.getMotCommande().equals("jeter")) {
			throw new IllegalArgumentException("Le mot-cle de commande passe doit etre 'jeter'");
		}
		if (!commande.aSecondMot()) // si le deuxieme mot du commande est null alors message d'erreur est affich�
		{
			System.out.println("Jeter quoi?");
			return;
		}

		String codeObjet = commande.getSecondMot();
		if (!joueur.contient(codeObjet)) // si le joueur ne passede pas objet du m�me code alors message d'erreur est
											// affich�
		{
			System.out.println("Vous ne possedez pas l'objet " + codeObjet);
			return;
		} else // sinon on enleve cet objet de la liste des objets de joueur et l'ajoute dans
				// la piece courante
		{
			ObjetZork oz = joueur.rechercherObjet(codeObjet);
			joueur.jeter(codeObjet);
			pieceCourante.ajouter(oz);
			System.out.println("L'objet " + codeObjet + " a ete j�t�.");
		}
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'jeterTous'. Le joueur
	 * tente de jeter tous les objets lesquelles il possede dans piece courante. Si
	 * la liste des objets de joueur est vide alors la message d'erreur sera
	 * affich�. Sinon ses objet seront retir� de la liste des objets Zork port� par
	 * joueur et ajout� dans la piece courante. Ce methode est appl� par methode
	 * "traiterCommande".
	 * 
	 * @ensures getJoueur().getObjets().isEmpty()
	 * @ensures getPieceCourante().getObjets().size() ==
	 *          \old(getPieceCourante().getObjets()).size() +
	 *          \old(getJoueur().getObjets()).size()
	 * @ensures getPieceCourante().getObjets().containsAll(\old(getJoueur().getObjets()))
	 */
	private void jeterTousObjets() {
		if (!joueur.getObjets().isEmpty()) {
			ArrayList<ObjetZork> objets = joueur.viderSac();
			pieceCourante.ajouterTous(objets);
			System.out.println("Les objets ont ete j�t�.");
		} else {
			System.out.println("Votre sac est vide.");
		}
	}

	/**
	 * Methode auxiliaire, qui permet d'executer la commande 'consulter'. Cette
	 * commande permet de voir les particularit�s des objets, de voir le prix d'un
	 * objet payant et de voir que ce que il y � l'interieur d'un objet compos�. Si
	 * le joueur consulte un objet compos� et celui-ci contient des objet Zork
	 * dedans, alors toutes ses objets Zork seront retir�s de la liste des objets du
	 * cet objet compos� et seront ajout�s dans la piece courante ou bien dans la
	 * listes des objets port�s par joueur si le contraintes du poids total sera
	 * respecter. Si le deuxieme mot du commande est null, ou si l'objet pareil
	 * n'existe pas ni dans la piece ni dans la liste des objets port�s par joueur
	 * allors message d'erreur sera affich�. Cas particulaire : pour consulter un
	 * objet dont le code correspend � "coffreFort" il faut d'abord avoir l'objet de
	 * type Cle dont le code correspond � "petitCle" dans la listes des objets Zork
	 * port� par joueur. Ce methode est appel� par methode "traiterCommande".
	 * 
	 * @param commande
	 *            Istance de la classe Commande, dont le second mot soit sp�cifi� le
	 *            code d'objet � consulter, soit est n'importe quelle autre chaine,
	 *            soit est null.
	 * 
	 * @requires commande!=null
	 * @requires commande.getMotCommande().equals("consulter")
	 * @ensures (!commande.getSecondMot().equals("coffreFort")) &&
	 *          (getPieceCourante().contient(commande.getSecondMot()) ||
	 *          getJoueur().contient(commande.getSecondMot())) ==> (\exisits
	 *          ObjetZork oz ; (oz instanceof ObjetCompose) &&
	 *          (getPieceCourante().getObjets().contains(oz) ||
	 *          ((getJoueur.getPoidsMax() >=
	 *          getJoueur.poidsTotal(getJoueur.getObjets()) + oz.getPoids()) &&
	 *          getJoueur().getObjets().contains(oz))) &&
	 *          oz.getCode().equals(commande.getSecondMot()) &&
	 *          !\old(oz).getObjets().isEmpty() ; oz.getObjets().isEmpty() &&
	 *          (getPieceCourante().getObjets().containsAll(\old(oz).getObjets()) ||
	 *          getJoueur().getObjets().containsAll(\old(oz).getObjets())))
	 * @throws NullPointerException
	 *             si la commande pass� est null.
	 * @throws IllegalArgumentException
	 *             si le mot-cl� de la commande pass� est different de 'consulter'
	 */
	private void consulterObjet(Commande commande) {
		if (commande == null) {
			throw new NullPointerException("La commande passe doit etre different de null.");
		}
		if (!commande.getMotCommande().equals("consulter")) {
			throw new IllegalArgumentException("Le mot-cle de commande passe doit etre 'consulter'");
		}
		if (!commande.aSecondMot()) // si le deuxieme mot du commande est null alors message d'erreur est affich�
		{
			System.out.println("Consulter quoi?");
			return;
		}

		String codeObjet = commande.getSecondMot();
		if (!joueur.contient(codeObjet) && !pieceCourante
				.contient(codeObjet)) /*
										 * si l'objet du m�me code, n'existe ni dans la liste des objets port�s par
										 * joueur ni dans la piece courante alors le message d'erreur sera affich�
										 */
		{
			System.out.println("Il y a pas un tel objet a consulter");
			return;
		} else if (joueur.contient(codeObjet)) // si le joueur possede tel objet
		{
			ObjetZork oz = joueur.rechercherObjet(codeObjet);
			if (oz instanceof ObjetCompose) // si cet objet est objet compos�
			{
				ObjetCompose oc = (ObjetCompose) oz;
				oc.consulter();
				ArrayList<ObjetZork> list = oc.getObjets();
				joueur.getObjets().addAll(list);
				list.clear();
			} else // sinon
			{
				oz.consulter();
			}
		}

		else // si l'objet est dans la piece
		{
			ObjetZork oz = pieceCourante.rechercherObjet(codeObjet);
			if (oz instanceof ObjetCompose) // si l'objet est objet compos�
			{
				ObjetCompose oc = (ObjetCompose) oz;
				/*
				 * cas particulaire: si objet compos� est un coffre-fort alors il faut d'abord
				 * avoir le petitCle pour le consulter
				 */
				if (oc.getCode().equals("coffreFort") && !joueur.contient("petitCle")) { // si joueur ne possede pas
																							// petitCle alors le message
																							// d'erreur est affich�
					System.out.println("Le coffre-fort est ferm�, trouvez le cle est consultez au nouveau");
					return;
				}
				oc.consulter();
				if (oc.getCode().equals("coffreFort")) /*
														 * si le joueur possede petitCle et il tente de consulter
														 * coffre-fort alors le petitCle est disparra�t
														 */
				{
					joueur.jeter("petitCle");
				}
				ArrayList<ObjetZork> list = oc.getObjets();
				pieceCourante.getObjets().addAll(list);
				list.clear();
			} else {
				oz.consulter();
			}

		}
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'acheter'. Si le joueur
	 * tente d'acheter un produit qui n'est pas dans la piece, ou si la commande ne
	 * possede pas le deuxieme mot, ou si il ne possede pas suffisament d'argent ou
	 * si il tente d'acheter l'objet qui n'est pas payant alors le message d'erreur
	 * est affich�. Sinon il l'achete et le nombre des pi�ces posed� par jouer est
	 * diminu� au m�me valeur que le prix du cet objet. Et en fin si la capacit�
	 * maximal de joueur lui permet de prendre l'objet alors il lui prend est cet
	 * objet est retir� de la piece courante. Sinon le message d'erreur est affich�.
	 * Ce methode est appel� par methode "traiterCommande".
	 * 
	 * @param commande
	 *            Istance de la classe Commande, dont le second mot soit sp�cifi� le
	 *            code d'objet � acheter, soit est n'importe quelle autre chaine,
	 *            soit est null.
	 * 
	 * @requires commande!=null
	 * @requires getMotCommande().equals("acheter")
	 * @ensures getPieceCourante().contient(commande.getSecondMot()) ==> (\forall
	 *          ObjetPayant op ; op.getCode.equals(commande.getSecondMot()) &&
	 *          \old(getPieceCourante()).contains(op) &&
	 *          \old(getJoueur()).contientCombienArgent()>= op.getPrix() &&
	 *          \old(getJoueur()).poidsTotal(\old(getJoueur()).getObjets())+op.getPoids()
	 *          <= getJoueur().getPoidsMax() ;
	 *          !getPieceCourante().contient(commande.getSecondMot()) &&
	 *          getJoueur().contient(commande.getSecondMot()))
	 * @throws NullPointerException
	 *             si la commande pass� est null.
	 * @throws IllegalArgumentException
	 *             si le mot-cl� de la commande pass� est different de 'acheter'
	 */
	private void acheterObjet(Commande commande) {
		if (commande == null) {
			throw new NullPointerException("La commande passe doit etre different de null.");
		}
		if (!commande.getMotCommande().equals("acheter")) {
			throw new IllegalArgumentException("Le mot-cle de commande passe doit etre 'acheter'");
		}
		if (!commande.aSecondMot()) // si le deuxieme mot du commande est null alors message d'erreur est affich�
		{
			System.out.println("Prendre quoi?");
			return;
		}

		String codeObjet = commande.getSecondMot();
		if (!pieceCourante.contient(codeObjet)) /*
												 * si l'objet du m�me code n'existe pas dans la piece courante alors le
												 * message d'erreur sera affich�
												 */
		{
			System.out.println("L'objet " + codeObjet + " n'existe pas dans cette piece");
			return;
		} else {
			ObjetZork oz = pieceCourante.rechercherObjet(codeObjet);
			if (!(oz instanceof ObjetPayant)) // si l'objet n'est pas payant alors le message d'erreur sera affich�
			{
				System.out.println(
						"Vous n'avez pas besoine d'acheter cet objet. Vous pouvais le prendre, tout simplement");
				return;
			} else {
				ObjetPayant op = (ObjetPayant) oz;
				if (op.isAchete()) {
					System.out.println("Ce objet est deja achete. Il suffut de lui prendre.");
					return;
				}
				if (op.getPrix() > joueur.contientCombienArgent()) /*
																	 * si le joueur ne possede suffisament d'argent
																	 * alors le message d'erreur sera affich�
																	 */
				{
					System.out.println("Vous n'avez pas suffisament d'argent");
					return;
				} else /*
						 * Sinon il l'achete et le nombre des pi�ces posed� par jouer est diminu� au
						 * m�me valeur que le prix du cet objet. L'objet est retir� de la piece et
						 * ajout� dans la liste des objets port�s par joueur
						 */
				{
					int cpt = op.getPrix();
					while (cpt > 0) {
						joueur.jeter("1euro");
						cpt--;
					}
					op.acheter();
					pieceCourante.retirer(codeObjet);
					joueur.prendre(op);
					System.out.println("Merci pour votre achat");
				}
			}
		}
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'retour'. Ce methode
	 * permet de revenir dans la piece pr�c�dement visit�. Si le joueur tente
	 * d'utiliser cette commande directement apres le debut de jeu (quand joueur n'a
	 * pas se deplac�) ou si il est viens d'entrer dans la piece de teleportation
	 * alors le message d'erreur est affich�. Ce methode utilise la direction oppos�
	 * (pour nord -sud et pour est-ouest) d'attribut "directionPrecedent" pour aller
	 * � la piece pr�c�dement visit�. Ce methode appele le methode auxilliaire qui
	 * se charge de deplacer tous les �tres dans jeu, et si la piece d'un �tre sera
	 * egale � la piece de joueur alors il posera une question. Ce methode est
	 * appel� par methode "traiterCommande".
	 * 
	 * @ensures getDirectionPrecedent()!=null &&
	 *          \old(getDirectionPrecedent()).equals("nord") ==>
	 *          getPieceCourante().equals(\old(getPieceCourante()).pieceSuivante("sud"))
	 * @ensures getDirectionPrecedent()!=null &&
	 *          \old(getDirectionPrecedent()).equals("sud") ==>
	 *          getPieceCourante().equals(\old(getPieceCourante()).pieceSuivante("nord"))
	 * @ensures getDirectionPrecedent()!=null &&
	 *          \old(getDirectionPrecedent()).equals("est") ==>
	 *          getPieceCourante().equals(\old(getPieceCourante()).pieceSuivante("ouest"))
	 * @ensures getDirectionPrecedent()!=null &&
	 *          \old(getDirectionPrecedent()).equals("ouest") ==>
	 *          getPieceCourante().equals(\old(getPieceCourante()).pieceSuivante("est"))
	 */
	private boolean retourDansPiecePrecedent() {
		if (directionPrecedente == null) // si c'est notre premier piece (le jeu vient de demarer)
		{
			System.out.println("C'est votre premiere piece !");
			return false;
		}
		String direction = "";
		if (directionPrecedente.equals("nord")) {
			direction += "sud";
		} else if (directionPrecedente.equals("sud")) {
			direction += "nord";
		} else if (directionPrecedente.equals("ouest")) {
			direction = "est";
		} else if (directionPrecedente.equals("est")) {
			direction = "ouest";
		}
		Piece pieceSuivant = pieceCourante.pieceSuivante(direction);
		pieceCourante = pieceSuivant;
		directionPrecedente = direction;
		boolean tueParEtre = deplacementEtTestEtres();
		System.out.println(pieceCourante.descriptionComplet());
		return tueParEtre;
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'objets'. Il affiche
	 * toutes les objets port�s par le joueur actuelement. Ce methode fait appel au
	 * methode du joueur "descriptionObjets". Ce methode est appel� par methode
	 * "traiterCommande".
	 */
	private void afficherObjetsDeJoueur() {
		System.out.println(joueur.descriptionObjets());
	}

	/**
	 * Methode auxilliaire qui permet d'executer la commande 'quitter'. Si le joueur
	 * tente de quitter le jeu alors il est perdant. Si on donne le deuxieme mot
	 * pour cette commande alors le message d'erreur est affich�. Ce methode est
	 * appel� par methode "traiterCommande".
	 * 
	 * @param commande
	 *            Istance de la classe Commande, dont le second mot soit est
	 *            n'importe quelle chaine, soit est null.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i> si l'utilisateur ne donne pas le deuxieme mot.<br>
	 *         <i>false</i> sinon.
	 * 
	 * @requires commande!=null
	 * @requires getMotCommande().equals("quitter")
	 * @ensures \result <==> !commande.aSecondMot()
	 * @throws NullPointerException
	 *             si la commande pass� est null.
	 * @throws IllegalArgumentException
	 *             si le mot-cl� de la commande pass� est different de 'quitter'
	 * @pure
	 */
	public boolean quitterJeu(Commande commande) {
		if (commande == null) {
			throw new NullPointerException("La commande passe doit etre different de null.");
		}
		if (!commande.getMotCommande().equals("quitter")) {
			throw new IllegalArgumentException("Le mot-cle de commande passe doit etre 'quitter'");
		}
		if (commande.aSecondMot()) // si l'utilisateur donne le deuxieme mot alors message d'erreur est affich�
		{
			System.out.println("Ecrivez le mot cle 'quitter' seulement pour finir le jeu");
			return false;
		} else // sinon, le jeu se termine. Le joueur est perdant
		{
			System.out.println("Vous etes perdu");
			return true;
		}
	}

	/**
	 * Methode auxillaire, qui est charg� de deplacer tous les �tres dans jeu. La
	 * conversation entre �tre et joueur se fait uniquement dans ce methode. Chaque
	 * fois quand ce methode est appel� tous les �tres se deplacent (ou bien ils
	 * restent sur place). Apres le deplacement le methode verefier si l'�tre se
	 * trouve dans une m�me piece que joueur. Si oui alors l'�tre pose lui une
	 * question. Si la reponse donn� par utilisateur est juste alors l'�tre lui
	 * laisse partir. Sinon il lui tue et le jeu se termine. Ce methode appele la
	 * methode qui permet de obtenir la commande � partir de la ligne entr� par
	 * utilisateur. Ce methode est appel� par les methodes
	 * "retourDansPiecePrecedent" et "deplacerVersAutrePiece".
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i> si la reponse de joueur � une question est fautive c'est
	 *         ce que m�ne vers la fin du jeu.<br>
	 *         <i>false</i> si le joueur n'a pas rencontr� aucun �tre ou si la
	 *         reponse de joueur � une quesetion est juste.
	 *
	 * @ensures (\forall Etre e; getEtres().contains(e);
	 *          !e.getPieceCourante().equals(getJoueur().getPieceCourante())) ==>
	 *          !\result
	 */
	private boolean deplacementEtTestEtres() {
		for (Etre etre : etres) {
			etre.deplacerVersAutrePiece();
			// si le joueur et l'�tre se trouve dans une piece
			if (etre.getPieceCourante().equals(pieceCourante)) {
				System.out.println("\nVous avez rencotr� " + etre.getNom() + ". Il vous dit :");
				int numeroQuestion = etre.poserQuestion();
				boolean syntaxeCorrecte = false;
				Commande commandeReponse = analyseurSyntaxique.getCommande();
				while (!syntaxeCorrecte) {
					if (commandeReponse.getMotCommande() == null
							|| !commandeReponse.getMotCommande().equals("repondre")) {
						System.out.println(
								etre.getNom() + " dit :\nVous devez rependre en tappant le mot cle 'repondre'.");
						commandeReponse = analyseurSyntaxique.getCommande();
					} else if (!commandeReponse.aSecondMot() || !commandeReponse.getSecondMot().equals(etre.getNom())) {
						System.out.println(
								etre.getNom() + " dit :\n" + commandeReponse.getSecondMot() + " n'est pas mon nom.");
						commandeReponse = analyseurSyntaxique.getCommande();
					} else if (!commandeReponse.aThirdMot() || !commandeReponse.getThirdMot().equals("a")
							&& !commandeReponse.getThirdMot().equals("b") && !commandeReponse.getThirdMot().equals("c")
							&& !commandeReponse.getThirdMot().equals("d")) {
						System.out.println(etre.getNom()
								+ " dit :\nDonnez votre reponse en iniquant que la lettre de reponse ('a', 'b', 'c' ou 'd').");
						commandeReponse = analyseurSyntaxique.getCommande();
					} else {
						syntaxeCorrecte = true;
					}
				}
				String reponse = commandeReponse.getThirdMot();
				if (etre.verefierReponse(numeroQuestion, reponse)) {
					System.out.println(
							etre.getNom() + " dit :\nC'est une reponse juste. Je vous laisse partir. Bon courage.");
				} else {
					System.out.println(etre.getNom() + " vous a tu�. Vous etes perdu.");
					return true;
				}
			}
		}
		return false;
	}
}