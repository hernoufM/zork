/**
 * La classe abstraite qui d�finit les objets Zork particulaires.
 * 
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Un "objet Zork particulaire" est un variation d'objet Zork. Cette classe
 * h�rite toutes les attributs et toutes les methodes du classe ObjetZork car,
 * en effet, objet Zork particulaire est un objet Zork (c'est � dire que il
 * possede le code, la description, le poids et la transportabilit�), sauf il a
 * des attribut ou des methodes complementaire. Cette classe se decompose sur :
 * les objets de type Cle, les objets de type ObjetPayant et les objet de type
 * ObjetComposee. Cette classe oblige ses sous-classes (classe Cle, classe
 * ObjetPayant et la classe ObjetCompose) de r�d�finir le methode "consulter".
 * 
 * Le joueur peut prendre et jeter les objets particulaire qui sont
 * transportable. Il peut aussi consulter toutes les objets particulaires
 * (rechercher quelque chose dedans, voir la particularit�). Les objets payants
 * sont exceptionnels car le joueur ne peut pas les prendre, il doit d'abord
 * l'acheter.
 * 
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 * 
 * @invariant this.getCode()!=null
 * @invariant this.getDescription()!=null
 * @invariant this.getPoids() >= 0
 * @invariant !ozp1.equals(ozp2) && (ozp1.isTransportable() ||
 *            ozp2.isTransportable) ==> !ozp1.getCode().equals(ozp2.getCode())
 */
public abstract class ObjetZorkParticulaire extends ObjetZork {
	/**
	 * Constructeur, qui creer l'objet Zork particulaire et initialise ces attributs
	 * par les valeurs specifi�.
	 *
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'un objet qu'on veut
	 *            creer.
	 * @param descrpt
	 *            Chaine de caracteres, qui represente petite description d'un objet
	 *            qu'on veut creer.
	 * @param poids
	 *            Entier positif, qui represente le poinds en kg d'un objet qu'on
	 *            veut creer.
	 * @param transportable
	 *            Boolean, qui dit pourrait-il joueur � transporter cet objet.
	 * 
	 * @requires code!=null
	 * @requires descrpt!=null
	 * @requires poids >= 0
	 * @ensures getCode().equals(code)
	 * @ensures getDescription().equals(descrpt)
	 * @ensures getPoids()==poids
	 * @ensures isTransportable() <==> transportable
	 * @throws NullPointerException
	 *             si le code et la description d'un objet pass� sont null.
	 * @throws IllegalArgumentException
	 *             si le poids d'un objet pass� est strictement inferieur � 0.
	 */
	public ObjetZorkParticulaire(String code, String descrpt, int poids, boolean transportable) {
		super(code, descrpt, poids, transportable);
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux objets Zork
	 * particulaire.
	 * 
	 * @param o
	 *            Instance de la class Object, avec laquelle on va comparer notre
	 *            objet.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux objets Zork particulaires ont le contenu
	 *         identiques <br>
	 *         <i>false</i>, si les deux objets Zork particulaires ont les contenus
	 *         differentes ou si "o" n'est pas une instance de la classe
	 *         ObjetZorkParticulaire.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof ObjetZorkParticulaire) ==> !\result
	 * @ensures (o instanceof ObjetZorkParticulaire) ==> \result <==>
	 *          (this.getCode().equals(((ObjetZork) o).getCode()) &&
	 *          this.getDescription().equals(((ObjetZork) o).getDescription()) &&
	 *          this.isTransportable() == ((ObjetZork) o).isTransportable() &&
	 *          this.getPoids() == ((ObjetZork) o).getPoids())
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof ObjetZorkParticulaire)) {
			return false;
		}
		return super.equals(o);
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * objets sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de cet objet Zork particulaire.
	 * 
	 * @ensures (\forall ObjetZorkPrticulaire ozp1,ozp2 ; ozp1.equals(ozp2) ;
	 *          ozp1.hashCode() == ozp2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return 5 * super.hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cet objet, tel
	 * que si et seulement si deux objet sont �gaux alors ils ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente cet objet Zork particulaire.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall ObjetZork Particulaire ozp1,ozp2 ; ozp1.equals(ozp2) ;
	 *          ozp1.toString().equals(ozp2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String str = "ObjetZorkParticulaire(" + getCode() + ") description = " + getDescription() + " poids = "
				+ getPoids() + " transportabilite = " + isTransportable();
		return str;
	}

	/**
	 * Methode abstrait, qui doit �tre r�d�finit dans les sous-classes (classe Cle,
	 * classe ObjetPayant et la classe ObjetComposee). Ce methode nous permet
	 * d'executer la commande 'consulter' sur l'objet particulaire.
	 */
	public abstract void consulter();
}
