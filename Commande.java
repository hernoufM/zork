/**
 * La classe qui d�finit les commandes dans le jeu "Zork".
 * 
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 *
 * Cette classe r�pertorie les informations li�es a une commande entr�e par
 * l'utilisateur. Une commande est constitu�e de trois chaines de caract�res: un
 * mot-cl� de commande, et deuxieme et troisieme mot apportants des compl�ments
 * (des param�tres) au mot-cl� indiquants la commande a ex�cuter (par exemple,
 * si la commande entr�e par l'utilisateur est "repondre fantome a", alors les
 * trois chaines de caract�res sont "repondre", "fantome" et "a").
 *
 * Les mots utilis�s lors de l'initialisation d'une instance de cette classe
 * sont suppos�s venir d'une commande utilisateur dont la validit� a d�j� �t�
 * test�e:
 * <ul>
 * <li>si le mot commande entr� par l'utilisateur ne correspond pas a une
 * commande valide, alors la valeur du mot commande donn� � l'initialisation
 * doit etre null</li>
 * <li>si la commande entr�e par l'utilisateur ne contient pas d'autre mot que
 * le mot commande, alors la valeur du deuxieme et troiseme mots donn�s �
 * l'initialisation doivent etre nulls</li>
 * <li>si utilisateur a donn� que deux mots alors le troisieme mot donn� �
 * l'initialisation doit etre null</li>
 * </ul>
 * La validit� du deuxieme et troisieme mots n'est pas test�e, ses valeurs
 * peuvent etre quelconque. Les autres mot entr�s par utilissateur dans terminal
 * seront ignor�s.
 * 
 * Cette classe contient des methodes qui permettent de consulter le premier mot
 * (mot-cl� de commande), deuxieme mot (param�tre) et troisieme mot (param�tre)
 * et qui permettent de conna�tre si le mot-cl� ou les param�tres sont nulls.
 * 
 * La classe Commande est utilis� lors d'interpretation des lignes entr� par
 * l'uitilisateur, et lors d'execution des commandes.
 *
 * @author Michael Kolling
 * @author Marc Champesme (pour la traduction francaise)
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 * 
 * @invariant (isInconnue()) ? getModCommande() == null : (\forall
 *            MotCleCommande mcc ; mcc!=null ;
 *            mcc.estCommande(getMotCommande()))
 * @invariant !aSecondMot() <==> getSecondMot() == null
 * @invariant !aThirdMot() <==> getThirdMot() == null
 */

public class Commande {
	/**
	 * Chaine de caracteres qui correspond au premier mot. Le premier mot soit
	 * correspond � la commande valide dans le jeu "Zork", soit est null.
	 */
	private String motCommande;

	/**
	 * Chaine de caracteres qui correspond au deuxieme mot. Le deuxieme mot soit
	 * correspond au param�tre du premier mot (mot-cl� de commande), par exemple: le
	 * code d'un objet, la destination de la sortie etc. Soit est n'importe quel
	 * autre mot. Soit est null.
	 */
	private String secondMot;

	/**
	 * Chaine de caracteres qui correspond au troisieme mot. Le troisieme mot soit
	 * correspond au param�tre du premier mot (mot-cl� de commande), par exemple: le
	 * nom d'un monstre, la destination de la sortie etc. Soit est n'importe quel
	 * autre mot. Soit est null.
	 */
	private String thirdMot;

	/**
	 * Constructeur, qui initialise une Commande � partir des trois mots sp�cifi�s.
	 *
	 * Le premier argument repr�sente un mot commande, sa valeur peut etre null si
	 * le mot commande ne correspond pas a une commande valide. Les deuxieme et
	 * troisieme mots peuvent �galement etre nulls dans le cas o� l'utilisateur
	 * n'aurait pas fourni l'un d'eux.
	 *
	 * @param motCommande
	 *            Chaine de caracteres, qui soit correspond � la commande valide
	 *            soit est null.
	 * @param secondMot
	 *            Chaine de caracteres, qui soit correspond au premier param�tre du
	 *            mot-cl� commande, soit est n'importe quel autre mot, soit est
	 *            null.
	 * @param thirdMot
	 *            Chaine de caracteres, qui soit correspond au deuxieme param�tre du
	 *            mot-cl� commande, soit est n'importe quel autre mot, soit est
	 *            null.
	 * 
	 * @requires (motCommande == null) || (\forall MotCleCommande mcc ; mcc!=null ;
	 *           mcc.estCommande(motCommande))
	 * @ensures getMotCommande().equals(motCommande)
	 * @ensures getSecondMot().equals(secondMot)
	 * @ensures getThirdMot().equals(thirdMot)
	 * @ensures isInconnue() <==> motCommande == null
	 * @ensures aSecondMot() <==> secondMot != null
	 * @ensures aThirdMot() <==> thirdMot != null
	 * @throws IllegalArgumentException
	 *             si le mot commande est different de null et ne correspond �
	 *             aucune commande autoris�.
	 */
	public Commande(String motCommande, String secondMot, String thirdMot) {
		MotCleCommande mcc = new MotCleCommande();
		if (motCommande != null && !mcc.estCommande(motCommande)) {
			throw new IllegalArgumentException("Le mot cle ne correspond pas � commande autoris�");
		}
		this.motCommande = motCommande;
		this.secondMot = secondMot;
		this.thirdMot = thirdMot;
	}

	/**
	 * Methode, qui renvoie le mot commande (le premier mot) de cette Commande. Si
	 * cette commande n'est pas une commande valide, la valeur renvoy�e est null.
	 *
	 * @return Chaine de caracteres, qui soit correspond au mot-cl� commande de
	 *         cette Commande, soit est null.
	 * 
	 * @ensures (\result == null) || (\forall MotCleCommande mcc ; mcc!=null ;
	 *          mcc.estCommande(\result))
	 * @pure
	 */
	public String getMotCommande() {
		return motCommande;
	}

	/**
	 * Methode, qui renvoie le dexuieme mot de cette Commande ou null si cette
	 * commande ne poss�de pas du deuxieme mot.
	 *
	 * @return Chaine de caracteres, qui soit correspond au premier param�tre du
	 *         mot-cl� de commande, soit est n'importe quel autre mot, soit est
	 *         null.
	 * 
	 * @pure
	 */
	public String getSecondMot() {
		return secondMot;
	}

	/**
	 * Methode, qui renvoie le troisieme mot de cette Commande ou null si cette
	 * commande ne poss�de pas du troisieme mot.
	 *
	 * @return Chaine de caracteres, qui soit correspond au deuxieme param�tre du
	 *         mot-cl� de commande, soit est n'importe quel autre mot, soit est
	 *         null.
	 * 
	 * @pure
	 */
	public String getThirdMot() {
		return thirdMot;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux commandes.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux commandes ont le contenu identique <br>
	 *         <i>false</i>, si les deux commandes ont le contenu differentes ou si
	 *         "o" n'est pas une instance de la classe Commande.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof Commande) ==> !\result
	 * @ensures (o instanceof Commande) ==> \result <==>
	 *          (this.getMotCommande().equals(((Commande) o).getMotCommande()) &&
	 *          this.getSecondMot().equals(((Commande) o).getSecondMot()) &&
	 *          this.getThirdMot().equals(((Commande) o).getThirdMot()))
	 * @throws NullPointerException
	 *             si l'objet passe est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof Commande))
			return false;
		return (this.motCommande.equals(((Commande) o).motCommande) && this.secondMot.equals(((Commande) o).secondMot)
				&& this.thirdMot.equals(((Commande) o).thirdMot));
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * commandes sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de cette commande.
	 * 
	 * @ensures (\forall Commande c1,c2 ; c1.equals(c2) ; c1.hashCode() ==
	 *          c2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		int hCode = 5 * (motCommande.hashCode() - secondMot.hashCode());
		return hCode;
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cette commande,
	 * tel que si et seulement si deux commandes sont �gaux alors ils ont la m�me
	 * chaine.
	 * 
	 * @return Chaine de caractere, qui represente cet commande.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall Commande c1,c2 ; c1.equals(c2) ;
	 *          c1.toString().equals(c2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "Mot-Cl� commande = " + motCommande + ", second mot = " + secondMot;
		return s;
	}

	/**
	 * Methode, qui teste si le mot-cl� commande est une commande reconnue par le
	 * jeu "Zork".
	 *
	 * @return Boolean, qui est : <i>true</i> si mot-cl� commande est null (cette
	 *         commande n'est pas valide) <i>false</i> sinon (commande est valide).
	 * 
	 * @ensures \result <==> getMotCommande()==null
	 * @pure
	 */
	public boolean isInconnue() {
		return (motCommande == null);
	}

	/**
	 * Methode, qui teste si l'utilisateur a entr� le deuxieme mot (teste si cette
	 * commande possede deuxieme mot).
	 *
	 * @return Boolean, qui est : <true> si cette commande poss�de un deuxieme mot
	 *         <false> sinon.
	 *
	 * @ensures \result <==> getSecondMot()!=null
	 * @pure
	 */
	public boolean aSecondMot() {
		return (secondMot != null);
	}

	/**
	 * Methode, qui teste si l'utilisateur a entr� le troisieme mot (teste si cette
	 * commande possede troisieme mot).
	 *
	 * @return Boolean, qui est : <true> si cette commande poss�de un troisieme mot
	 *         <false> sinon.
	 *
	 * @ensures \result <==> getThirdMot()!=null
	 * @pure
	 */
	public boolean aThirdMot() {
		return (thirdMot != null);
	}
}
