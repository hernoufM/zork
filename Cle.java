/**
 * La classe qui d�finit les cl�s dans le jeu "Zork".
 * 
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Les cl�s sont parmi les objets Zork particulaires (les autres sont les objets
 * payants et les objet compos�s). Ils ont les m�me caracteristiques et le m�me
 * compertement que l'objet Zork, sauf que le cl� est toujours transportable et
 * il a le poids null. En plus la classe Cle r�d�finit le methode "consulter".
 * 
 * Les cl� sont une partie indispensable de jeu. Le but de jeu est de trouver 5
 * cl�s, qui permettront � joueur de sortir de l'univesrit� et de gagner. Ces
 * cl�s ont ces attributs : Le code contient le mot "cle" suivi d'une chiffre de
 * 1 � 5, qui id�ntifie chaque cl� deu maniere unique. La description contient
 * phrase "Cle de la sortie". Ils existent aussi des autres type des cl�s. Ils
 * permettront � joueur d'ouvrir une porte, une tiroir etc.
 * 
 * Le joueur peut prendre, jeter et consulter les cl�s (c'est � dire voir la
 * particularit�). Il peut transporter le nombre illimit� des cl�s.
 *
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 * 
 * @invariant this.getCode()!=null
 * @invariant this.getDescription()!=null
 * @invariant this.getPoids() == 0
 * @invariant this.isTransportable()
 * @invariant !c1.equals(c2) ==> !c1.getCode().equals(c2.getCode())
 */
public class Cle extends ObjetZorkParticulaire {

	/**
	 * Constructeur, qui creer la cl� et initialise ces attributs par les valeurs
	 * specifi�.
	 *
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'une cl� qu'on veut
	 *            creer.
	 * @param descrpt
	 *            Chaine de caracteres, qui represente petite description d'une cl�
	 *            qu'on veut creer.
	 * 
	 * @requires code!=null
	 * @requires descrpt!=null
	 * @ensures getCode().equals(code)
	 * @ensures getDescription().equals(descrpt)
	 * @throws NullPointerException
	 *             si le code et la description d'un objet pass� sont null.
	 */
	public Cle(String code, String descrpt) {
		super(code, descrpt, 0, true);
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux cl�s.
	 * 
	 * @param o
	 *            Instance de la class Object, avec laquelle on va comparer notre
	 *            cl�.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux cl�s ont le contenu identique <br>
	 *         <i>false</i>, si les deux cl�s ont les contenus differentes ou si "o"
	 *         n'est pas une instance de la classe Cle.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof Cle) ==> !\result
	 * @ensures (o instanceof Cle) ==> \result <==>
	 *          (this.getCode().equals(((ObjetZork) o).getCode()) &&
	 *          this.getDescription().equals(((ObjetZork) o).getDescription()))
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof Cle))
			return false;
		return super.equals(o);
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * cl�s sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de cette cl�.
	 * 
	 * @ensures (\forall Cle c1,c2 ; c1.equals(c2) ; c1.hashCode() == c2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return 10 * super.hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cette cl�, tel
	 * que si et seulement si deux cl�s sont �gaux alors ils ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente cette cl�.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall Cle c1,c2 ; c1.equals(c2) ;
	 *          c1.toString().equals(c2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "Cle (" + getCode() + ") : " + getDescription();
		s += " poids = 0";
		s += " transportable";
		return s;
	}

	/**
	 * R�d�finition du methode "consulter" qui nous permet d'executer la commande
	 * 'consulter' sur la cl�.
	 * 
	 * @override
	 */
	public void consulter() {
		System.out.println(this.getDescription());
		if (this.getDescription().equals("Cle de la sortie")) // si cette cl� est une cl� de la sortie
		{
			System.out.println("Vous devez garder toutes les cles pour pouvoir sortir de l'universite");
		} else // sinon
		{
			System.out.println("Vous devez ouvrir quelque chose avec cette cle");
		}
	}

}
