/**
 * La classe qui d�finit les questions, ses options et reponse. Questions sont
 * poss�es par �tres (Fantom ou Demon) dans le jeu "Zork".
 *
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Cette classe permet de structurer la question, pour facilit� de leur
 * utilisation. Les questions sont poss� par � joueur des que il rentre dans
 * m�me piece que l'�tre. Chaque �tre possede son propre liste des questions qui
 * sont li� � sa ma�trise. Par exemple, un fantome ma�trise la geographie, alors
 * ses questions sont aussi � propos de la geographie. Les questions sont
 * represent�es sou forme d'une quiz, c'est � dire que pour une question il y a
 * 4 options et une seule reponse.
 * 
 * Chaque question possede une interrogation constante qui represente la
 * question lui m�me. Par exemple, "Combien il y a des os dans le corps?". Pour
 * chaque interrogation, il y a 4 option constante. Une option est juste et les
 * autre sont fautives. Les options sont nomm� de fa�ons optionX, o� X est
 * lettre de A � B. Pour stocker la reponse il suffit de stocker la lettre
 * constante d'une option qui est juste. Cette lettre est aussi constante. Par
 * exemple, la question "Quel est capital de la France?" possede 4 options:
 * optionA: Berlin, optionB : New York, optionC : Paris, optionD : Londres.
 * L'optionC est juste, alors variable 'reponse' va contenir chaine "c".
 * 
 * Classe possede des methodes qui permettent de consulter la question, les
 * options et la reponse et methode qui permet de comparer la reponse donn� par
 * utilisateur et la reponse r�elle.
 * 
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 *
 * @invariant getQuestion()!=null
 * @invariant getOptionA()!=null
 * @invariant getOptionB()!=null
 * @invariant getOptionC()!=null
 * @invariant getOptionD()!=null
 * @invariant getReponse().equals("a") || getReponse().equals("b") ||
 *            getReponse().equals("c") || getReponse().equals("d")
 * @invariant (\forall String str; str.equals(getReponse()) ;
 *            verifierReponse(str))
 */
public class Question {
	/**
	 * Chaine de caracteres, qui correspond � question. Cette chaine est
	 * non-modifiable et non-null.
	 */
	private final String question;

	/**
	 * Chaine de caractere, qui correspond � premier option, qui peut etre juste.
	 * Cette chaine est non-modifiable et non-null.
	 */
	private final String optionA;

	/**
	 * Chaine de caractere, qui correspond � deuxieme option, qui peut etre juste.
	 * Cette chaine est non-modifiable et non-null.
	 */
	private final String optionB;

	/**
	 * Chaine de caractere, qui correspond � troisieme option, qui peut etre juste.
	 * Cette chaine est non-modifiable et non-null.
	 */
	private final String optionC;

	/**
	 * Chaine de caractere, qui correspond � quatrieme option, qui peut etre juste.
	 * Cette chaine est non-modifiable et non-null.
	 */
	private final String optionD;

	/**
	 * Chaine de caractere, qui correspond � lettre de l'option qui est juste ("a",
	 * "b", "c", "d"). Cette chaine est non-modifiable et non-null.
	 */
	private final String reponse;

	/**
	 * Constructeur, qui creer et initialise une instance de la classe Question en
	 * donnant sa question, ses options et sa reponse.
	 * 
	 * @param question
	 *            Chaine de caractere, qui represente une question. Doit etre
	 *            different de null.
	 * @param a
	 *            Chaine de caractere, qui represente premiere option de la question
	 *            specifi�. Doit etre different de null.
	 * @param b
	 *            Chaine de caractere, qui represente deuxieme option de la question
	 *            specifi�. Doit etre different de null.
	 * @param c
	 *            Chaine de caractere, qui represente troisieme option de la
	 *            question specifi�. Doit etre different de null.
	 * @param d
	 *            Chaine de caractere, qui represente quatrieme option de la
	 *            question specifi�. Doit etre different de null.
	 * @param reponse
	 *            Chaine de caractere, qui represente la lettre de l'option qui est
	 *            juste. La valeur possible est soit "a", soit "b", soit "c" ou soit
	 *            "d". Doit etre different de null.
	 * 
	 * @requires question != null
	 * @requires a != null
	 * @requires b != null
	 * @requires c != null
	 * @requires d != null
	 * @requires reponse.equals("a") || reponse.equals("b") || reponse.equals("c")
	 *           || reponse.equals("d")
	 * @ensures getQuestion.equals(question)
	 * @ensures getOptionA.equals(a)
	 * @ensures getOptionB.equals(b)
	 * @ensures getOptionC.equals(c)
	 * @ensures getOptionD.equals(d)
	 * @ensures getReponse.equals(reponse)
	 * @throws NullPointerException
	 *             Si la question ou une parmi les options pass� sont nulls.
	 * @throws IllegalArgumentException
	 *             Si la reponse pass� a une autre valeur que "a" ou "b" ou "c" ou
	 *             "d".
	 */
	public Question(String question, String a, String b, String c, String d, String reponse) {
		if (question == null || a == null || b == null || c == null || d == null) {
			throw new NullPointerException("La question et ses options doivent etre different de null.");
		}
		if (!reponse.equals("a") && !reponse.equals("b") && !reponse.equals("c") && !reponse.equals("d")) {
			throw new IllegalArgumentException("Les valeurs possibles pour reponse : a, b, c, d");
		}
		this.question = question;
		optionA = a;
		optionB = b;
		optionC = c;
		optionD = d;
		this.reponse = reponse;
	}

	/**
	 * Methode, qui permet de savoir quelle est la question de cette objet.
	 * 
	 * @return Chaine de caracteres, qui correspond � question de cette objet. Doit
	 *         etre non null.
	 * 
	 * @ensures \result != null
	 * @pure
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * Methode, qui permet de savoir quelle est la premiere option de cette
	 * question.
	 * 
	 * @return Chaine de caracteres, qui correspond � premiere option de cette
	 *         question. Doit etre non null.
	 * 
	 * @ensures \result != null
	 * @pure
	 */
	public String getOptionA() {
		return optionA;
	}

	/**
	 * Methode, qui permet de savoir quelle est la deuxieme option de cette
	 * question.
	 * 
	 * @return Chaine de caracteres, qui correspond � deuxieme option de cette
	 *         question. Doit etre non null.
	 * 
	 * @ensures \result != null
	 * @pure
	 */
	public String getOptionB() {
		return optionB;
	}

	/**
	 * Methode, qui permet de savoir quelle est la troisieme option de cette
	 * question.
	 * 
	 * @return Chaine de caracteres, qui correspond � troisieme option de cette
	 *         question. Doit etre non null.
	 * 
	 * @ensures \result != null
	 * @pure
	 */
	public String getOptionC() {
		return optionC;
	}

	/**
	 * Methode, qui permet de savoir quelle est la quatrieme option de cette
	 * question.
	 * 
	 * @return Chaine de caracteres, qui correspond � quatrieme option de cette
	 *         question. Doit etre non null.
	 * 
	 * @ensures \result != null
	 * @pure
	 */
	public String getOptionD() {
		return optionD;
	}

	/**
	 * Methode, qui permet de savoir quelle est la lettre de l'option qui est juste
	 * ("a", "b", "c", "d").
	 * 
	 * @return Chaine de caracteres, qui correspond � lettre de l'option qui est
	 *         juste. La valeur doit etre soit "a" soit "b" soit "c" soit "d".
	 * 
	 * @ensures \result.equals("a") || \result.equals("b") || \result.equals("c") ||
	 *          \result.equals("d")
	 * @pure
	 */
	public String getReponse() {
		return reponse;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux questions avec
	 * ses options et reponses.
	 * 
	 * @param o
	 *            Instance de la classe Object, avec laquelle on va comparer notre
	 *            instance de la classe Question.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux questions ont le contenu identique <br>
	 *         <i>false</i>, si les deux questions ont les contenus differentes ou
	 *         si "o" n'est pas une instance de la classe Question.
	 * 
	 * @requires o!=null
	 * @ensures (!(o instanceof Question)) ==> !\result
	 * @ensures (o instanceof Question) ==> \result <==>
	 *          (getQuestion().equals(((Question) o).getQuestion()) &&
	 *          getOptionA().equals(((Question) o).getOptionA()) &&
	 *          getOptionB().equals(((Question) o).getOptionB()) &&
	 *          getOptionC().equals(((Question) o).getOptionC()) &&
	 *          getOptionD().equals(((Question) o).getOptionD()) &&
	 *          getReponse().equals(((Question) o).getReponse()))
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof Question)) {
			return false;
		}
		Question q = (Question) o;
		return (question.equals(q.question) && optionA.equals(q.optionA) && optionB.equals(q.optionB)
				&& optionC.equals(q.optionC) && optionD.equals(optionD) && reponse.equals(q.reponse));
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * questions sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de cette question.
	 * 
	 * @ensures (\forall Question q1,q2 ; q1.equals(q2) ; q1.hashCode() ==
	 *          q2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return question.hashCode() + optionA.hashCode() + optionB.hashCode() + optionC.hashCode() + optionD.hashCode()
				+ 10 * reponse.hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cette question,
	 * tel que si et seulement si deux question sont �gaux alors ils ont la m�me
	 * chaine.
	 * 
	 * @return Chaine de caractere, qui represente cette question.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall Question q1,q2 ; q1.equals(q2) ;
	 *          q1.toString().equals(q2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String str = "Question question = " + question + " optionA = " + optionA + " optionB = " + optionB
				+ " optionC = " + optionC + " optionD = " + optionD + " reponse = " + reponse;
		return str;
	}

	/**
	 * Methode, qui compare la reponse pass� avec la reponse r�elle de cette
	 * question. Si ils sont identique alors methode renvoie true sinon false.
	 * 
	 * @param reponse
	 *            Chaine de caractere, qui represente la reponse � comparer. Doit
	 *            etre different de null.
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si la reponse pass� correspond � reponse r�elle de la
	 *         question.<br>
	 *         <i>false</i>, sinon.
	 * 
	 * @requires reponse != null
	 * @ensures \result <==> reponse.equals(getReponse())
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @pure
	 */
	public boolean verifierReponse(String reponse) {
		if (reponse == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		return this.reponse.equals(reponse);
	}
}
