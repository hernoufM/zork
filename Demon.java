import java.util.*;

/**
 * Classe, qui definit les demons dans le jeu "Zork".
 * 
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Les demons sont des �tres, �a veut dire que ils ont le comportement comme les
 * autres �tres (Fantome). Cette classe herite toutes les comportement de la
 * classe Etre : comme tous les autres �tres ils possedent le nom et il peuvent
 * se deplacer d'une piece � une autre. Toutes les demons ma�trisent que le
 * Culture generale. C'est � dire que tous les question sont � propos de Culture
 * generale. Les demons en plus possede la liste des questions, qui est
 * implement� par ArrayList<Question>. Chaque demon peut poser au plus 16
 * questions. Les questions sont initialis�es lors de creation du demon.
 * 
 * Cette classe possede des methode qui permet de consulter la liste des
 * questions. En plus cette classe redefini les methode abstraite de la
 * super-classe (Etre) qui permettent de poser la question ainsi que donner ses
 * options et qui permet de verefier la reponse donn� par joueur et supprimer
 * cette question de la liste (si la reponse est juste).
 * 
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 *
 * @invariant getNom()!=null
 * @invariant getMaitrise.equals("Culture generale")
 * @invariant getPieceCourante != null
 * @invariant getQuestions()!=null
 * @invariant getQuestions().size() >= 0 && getQuestions().size() <= 16
 * @invariant getQuestions().isEmpty() <==> poserQuestion() == -1
 * @invariant !getQuestions().isEmpty() <==> poserQuestions() >= 0 &&
 *            poserQuestions() < getQuestions().size()
 * @invariant getQuestions().isEmpty() ==> (\forall String reponse; reponse !=
 *            null ; verefierReponse(-1, reponse))
 * @invariant !getQuestions().isEmpty() ==> (\forall String reponse, int
 *            numeroQuestion; numeroQuestion == poserQuestion() &&
 *            reponse.equals(getQuestions().get(numeroQuestion).getReponse());
 *            verifier(numeroQuestion, reponse))
 * @invariant !getQuestions().isEmpty() ==> (\forall String reponse, int
 *            numeroQuestion; numeroQuestion == poserQuestion() &&
 *            !reponse.equals(getQuestions().get(numeroQuestion).getReponse());
 *            !verifier(numeroQuestion, reponse))
 */
public class Demon extends Etre {
	/**
	 * ArrayList des Questions, qui represente toutes les questions qui peuvent �tre
	 * pos�es par un demon.
	 */
	private ArrayList<Question> questionnaire;

	/**
	 * Constructeur, qui fait appele � constructeur de super classe (Etre). Ce
	 * constructeur en plus intitialise et rempli la liste des questions en ajoutant
	 * 16 questions differentes pour demon.
	 * 
	 * @param nom
	 *            Chaine de caracteres, qui correspend au nom d'un demon. Doit �tre
	 *            different de null.
	 * @param pieceInitiale
	 *            Instance de la classe Piece, qui correspend � piece initiale d'un
	 *            demon. Doit �tre differente de null.
	 * 
	 * @requires nom != null
	 * @requires pieceInitiale != null
	 * @ensures getNom().equals(nom)
	 * @ensures getMaitrise().equals("Culture generale")
	 * @ensures getPieceCourante().equals(pieceInitiale)
	 * @ensures getQuestions() != null
	 * @ensures getQuestions().size() == 16
	 * @throws NullPointerException
	 *             si le nom ou la piece initiale est null.
	 */
	public Demon(String nom, Piece pieceInitiale) {
		super(nom, "Culture generale", pieceInitiale);
		questionnaire = new ArrayList<Question>(16);
		questionnaire.add(new Question(
				"Dans quelle ville italienne se situe l�action de la piece de Shakespeare 'Romeo et Juliette'?",
				"Venise", "Milan", "Rome", "Verone", "d"));
		questionnaire.add(new Question("Dans quel pays peut-on trouver la Catalogne, l�Andalousie et la Castille?",
				"L'Espagne", "La France", "Le Portugal", "L'Italie", "a"));
		questionnaire.add(new Question("Parmi les animaux suivants, lequel peut se deplacer le plus rapidement?",
				"Le guepard", "Le springbok", "Le leopard", "Le chevreuil", "a"));
		questionnaire.add(new Question("Quel celebre dictateur dirigea l�URSS du debut des annees 1920 a 1953?",
				"Molotov", "Trotski", "Staline", "Lenine", "c"));
		questionnaire.add(new Question("Qui etait le dieu de la guerre dans la mythologie grecque?", "Apollon", "Ares",
				"Hermes", "Hades", "b"));
		questionnaire.add(new Question("Quel pays a remporte la coupe du monde de football en 2014?", "Le Bresil",
				"L'Italie", "L'Argentine", "L'Allemagne", "d"));
		questionnaire
				.add(new Question("Le drapeau russe est blanc, bleu et �?", "Jaune", "Rouge", "Noir", "Vert", "b"));
		questionnaire.add(new Question("Quel est le plus long fleuve en France?", "Le Rhin", "Le Rhone", "La Loire",
				"La Seine", "c"));
		questionnaire.add(new Question("A quel ecrivain attribue-t-on la redaction de l�Illiade et l�Odyssee?",
				"Virgile", "Homere", "Euripide", "Sophocle", "b"));
		questionnaire.add(new Question("Quel est l'album le plus vendu de l'Histoire?", "Back in Black des AC/DC",
				"Come on Over de Shania Twains", "Thriller de Michael Jackson", "Hotel California des Eagles", "c"));
		questionnaire.add(new Question("Dans quelle ville le Colisee (l'amphithe�tre Flavien) se trouve-t-il?",
				"Venise", "Rome", "Athenes", "Barcelone", "b"));
		questionnaire.add(new Question("Quel pays a decide par referendum de quitter l�Union europeenne?", "La Pologne",
				"La Suede", "L'Islande", "Le Royaume-Uni", "d"));
		questionnaire.add(new Question("Quel est le nom usuel d�un terrain de tennis?", "Le court", "La pelouse",
				"Le ground", "Le terrain", "a"));
		questionnaire.add(new Question("Quelle capitale actuelle a ete physiquement coupee en deux de 1961 a 1989?",
				"Prague", "Berlin", "Belgrade", "Tirana", "b"));
		questionnaire.add(new Question(
				"En quelle annee Neil Armstrong declare-t-il �C�est un petit pas pour l�homme, un bon de geant pour l�humanite�?",
				"1967", "1969", "1966", "1968", "b"));
		questionnaire.add(new Question("En 2016, quel acteur recoit l�Oscar du meilleur acteur?", "Michael Fassbender",
				"Matt Damon", "Bryan Cranston", "Leonardo di Caprio", "d"));
	}

	/**
	 * Methode, qui permet de consulter la liste des questions qui peuvent �tre pos�
	 * par demon.
	 * 
	 * @return L'instance du type ArrayList<Question>, qui represente la liste des
	 *         questions qui peuvent �tre pos� par demon.
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() >= 0 && \result.size <= 16
	 * @pure
	 */
	public ArrayList<Question> getQuestions() {
		return questionnaire;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux demons.
	 * 
	 * @param o
	 *            Instance de la classe Object, avec laquelle on va comparer notre
	 *            demon.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux demons ont le nom identique <br>
	 *         <i>false</i>, si les deux demons ont les noms differentes ou si "o"
	 *         n'est pas une instance de la classe Demon.
	 * 
	 * @requires o!=null
	 * @ensures (!(o instanceof Demon)) ==> !\result
	 * @ensures (o instanceof Demon) ==> \result <==> getNom().equals(((Demon)
	 *          o).getNom())
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof Demon)) {
			return false;
		}
		return super.equals(o);
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * demons sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de ce demon.
	 * 
	 * @ensures (\forall Demon d1,d2 ; d1.equals(d2) ; d1.hashCode() ==
	 *          d2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return 3 * super.hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cet demon, tel
	 * que si et seulement si deux demons sont �gaux alors ils ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente ce demon.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall Demon d1,d2 ; d1.equals(d2) ;
	 *          d1.toString().equals(d2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String str = "Demon nom = " + getNom() + " maitrise = " + getMaitrise();
		return str;
	}

	/**
	 * Redefinition du methode "poserQuestion". Ce methode doit appeler la methode
	 * "afficherMessage" et choisir la question dans la liste des questions de
	 * demon. Le choix est fait aleotoirement, gr�ce � instance de la classe Random
	 * dont methode a retourn� l'entier entre 0 et la taille de la liste. Apres il
	 * affiche la question et les options. Le numero de question est retourn�. Si il
	 * n'y a plus de question � posser le methode retourne -1.
	 * 
	 * @return Entier, qui represente le numero de question dans la liste des
	 *         question du demon. Doit �tre �gale � -1 si la liste est vide. Si la
	 *         liste n'est pas vide, la valeur retourn� doit �tre entre 0 et la
	 *         taille de la liste.
	 * 
	 * @ensures getQuestions().isEmpty() <==> \result == -1
	 * @ensures !getQuestions().isEmpty() <==> \result >= 0 && \result <
	 *          getQuestions().size()
	 * @override
	 * @pure
	 */
	public int poserQuestion() {
		afficherMessage();
		if (!questionnaire.isEmpty()) {
			Random rand = new Random();
			int numeroQuestion = rand.nextInt(questionnaire.size());
			Question question = questionnaire.get(numeroQuestion);
			System.out.println(question.getQuestion());
			System.out.println("a) " + question.getOptionA());
			System.out.println("b) " + question.getOptionB());
			System.out.println("c) " + question.getOptionC());
			System.out.println("d) " + question.getOptionD());
			return numeroQuestion;
		}
		System.out.println("J'ai vous deja posse tous les questions, alors je vous laisse partir.");
		return -1;
	}

	/**
	 * Redefinition du methode "verifierReponse". Ce methode permet de comparer la
	 * reponse donn� par utilisateur avec la reponse r�elle de question dont le
	 * numero dans la liste est specifi�. Le numero doit etre entre 0 et la taille
	 * de la liste. Si les reponses sont identiques, alors le methode supprime cette
	 * question � partir de la liste et retourne true, sinon false. Si le demon n'a
	 * pas posser la question, alors le numero de queston specifi� doit �tre -1 et
	 * le retourne sera toujours true.
	 * 
	 * @param numeroQuestion
	 *            Entier, qui represente le numero de question dans la liste des
	 *            question du demon. Doit etre entre 0 et la taille de la liste si
	 *            la liste n'est pas vide. Si la liste est vide doit �tre egale �
	 *            -1.
	 * @param reponse
	 *            Chaine de caracteres, qui correspond � reponse donn� par joueur de
	 *            la question dont le numero (numero dans la liste des question de
	 *            demon) est specifi�. Doit �tre different de null.
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si la reponse donn� par joueur et la reponse r�elle sont
	 *         identiques <br>
	 *         <i>false</i>, sinon.
	 * 
	 * @requires getQuestions().isEmpty() ==> numeroQuestion == -1
	 * @requires !getQuestions().isEmpty() ==> numeroQuestion >= 0 && numeroQuestion
	 *           < getQuestion().size()
	 * @requires reponse != null
	 * @ensures getQuestions().isEmpty() ==> \result
	 * @ensures !getQuestions().isEmpty() ==>
	 *          reponse.equals(getQuestions().get(numeroQuestion).getReponse()) <==>
	 *          \result
	 * @ensures !getQuestions().isEmpty() && \result ==> \old(getQuestions().size())
	 *          == getQuestions().size()+1
	 * @ensures getQuestions().isEmpty() || !\result ==> \old(getQuestions().size())
	 *          == getQuestions().size()
	 * @ensures !getQuestions().isEmpty() && \result ==> (\forall int i; i>=0 && i <
	 *          numeroQuestion;
	 *          getQuestions().get(i).equals(\old(getQuestions().get(i)))) &&
	 *          (\forall int j; j>=numeroQuestion && j < getQuestions().size();
	 *          getQuestions().get(j).equals(\old(getQuestions().get(j+1))))
	 * @ensures !getQuestions().isEmpty() && !\result ==> (\forall int i; i>=0 && i
	 *          < getQuestions().size();
	 *          getQuestions().get(i).equals(\old(getQuestions().get(i))))
	 * @throws IllegalArgumentException
	 *             dans le cas o� la liste est vide et le numero de la question
	 *             pass� est different de -1.
	 * @throws NullPointerException
	 *             si la reponse donn� est null.
	 * @throws IndexOutOfBounds
	 *             dans le cas o� la liste n'est pas vide et le numero de la
	 *             question pass� n'est pas entre 0 et la taille de la liste.
	 * @override
	 */
	public boolean verefierReponse(int numeroQuestion, String reponse) {
		if (reponse == null) {
			throw new NullPointerException("La reponse doit �tre differente de null");
		}
		if (questionnaire.isEmpty() && numeroQuestion != -1) {
			throw new IllegalArgumentException("Si la liste est vide le numero de question doit etre -1");
		}
		if (!questionnaire.isEmpty() && (numeroQuestion < 0 || numeroQuestion >= questionnaire.size())) {
			throw new IndexOutOfBoundsException(
					"Si la liste n'est pas vide le numero de question doit etre entre 0 et la taille de l liste");
		}
		if (numeroQuestion != -1) {
			Question question = questionnaire.get(numeroQuestion);
			if (!question.verifierReponse(reponse)) {
				return false;
			}
			questionnaire.remove(numeroQuestion);
		}
		return true;
	}

}