import java.util.*;

/**
 * La classe qui d�finit les pieces dans le jeu "Zork".
 *
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 *
 * Une piece represente un des lieux dans lesquels se d�roule l'action du jeu. �
 * un moment donn�, l'action se deroule dans une seule piece. Ils existent des
 * pieces, dont l'acc�s est autoris� si une certaine condition est verifi�e (par
 * exemple, si le joueur possede carte etudiante). La carte de toutes les piece
 * represente la carte de l'universit� Paris 13. Joueur et les �tres se
 * deplacent dans les pieces.
 * 
 * Chaque piece doit posseder une petite description. Description peut contenir
 * quelques mots sur ce que represente cette piece (par exemple "Restaurant
 * universitaire"), mais aussi la description peut contenir une message
 * important sur cette piece (par exemple "Bibliotheque. Attention, le chien
 * agressive. Il faut le nourir"). Certaines pieces posedent des objets dedans.
 * Chaque piece peut contenir le nombre illimit� des objets. Ces objets peuvent
 * �tre transportable ou non. Le joueur peut prendre les objets � partie de la
 * piece et jeter les objets dans la piece. Chaque piece est reli�e � au plus
 * quatre autres pieces par des sorties. Les sorties sont �tiquett�es "nord",
 * "est", "sud", "ouest". Pour chaque direction, la piece poss�de une r�f�rence
 * sur la piece voisine. Si la piece ne possede pas des sortie dans certaine
 * direction, alors cette r�f�rence est null. Chaque piece contient au moins une
 * sortie avant de la demarage de jeu.L'instance de la classe qui implemente
 * l'interface Map represente toutes les sorties de cette piece.
 * 
 * Cette classe contient des methodes qui permettent de manipuler des objets
 * (comme "ajouter", "retirer","rechercherObjet" etc) methode de manipulation
 * avec des sorties ("pieceSuivante") ainsi que les autres methodes auxiliaires.
 * 
 * Le methode "creerPieces" de la classe Jeu permet de cr�er et initialiser
 * toutes les pieces et aussi permet de cr�er et initialiser touts les objets
 * qui sont initialement dans ces pieces.
 *
 * @author Michael Kolling
 * @author Marc Champesme (pour la traduction francaise)
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 * 
 * @invariant getDescription()!=null
 * @invariant getObjets()!=null
 * @invariant getSorties()!=null
 * @invariant descriptionComplet()!=null
 */

public class Piece {
	/**
	 * Chaine de caracteres, qui contient petite description de cette piece.
	 */
	private String description;
	/**
	 * La piece contient un certaine nombre d'ojets. Ces objets sont stock�s dans
	 * une ArrayList qui contient des objets de type ObjetZork.
	 */
	private ArrayList<ObjetZork> objets;
	/**
	 * La piece peut contenir les sorties qui m�nent vers les autres pieces. Il y a
	 * 4 direction: nord, est, sud, ouest. Si piece possede sortie dans une parmi 4
	 * directions, cette sortie sera assosier a cette direction dans l'objet de type
	 * Map.
	 */
	private Map<String, Piece> sorties;

	/**
	 * Constructeur, qui initialise une piece et lui donne une courte description.
	 * Cette piece est initialis� sans objets et sans des pieces en voisinage. Les
	 * liste d'objets et l'instance de type Map sont initialis�s.
	 * 
	 * @param description
	 *            Chaine de caracteres, qui represente courte description d'une
	 *            piece. Si description est null alors on va creer piece avec
	 *            description par default.
	 * 
	 * @ensures (description == null) ? getDescription().equals("Piece") :
	 *          getDescription().equals(description)
	 * @ensures getObjets().isEmpty()
	 * @ensures getSorties().isEmpty()
	 */
	public Piece(String description) {
		if (description != null) {
			this.description = description;
		} else {
			this.description = "Piece";
		}
		objets = new ArrayList<ObjetZork>();
		sorties = new HashMap<String, Piece>();
	}

	/**
	 * Constructeur, qui initialise une piece avec des objets dedans et lui donne
	 * une description. L'instance de type Map est initialis�.
	 *
	 * @param description
	 *            Chaine de caracteres, qui represente courte description d'une
	 *            piece. Si description est null alors on va creer piece avec
	 *            description par default.
	 * 
	 * @param objets
	 *            ArrayList des objets de type ObjetZork, qui represente toutes les
	 *            objets presentent initialement dans la piece.
	 * 
	 * @requires objets!=null
	 * @ensures (description == null) ? getDescription().equals("Piece") :
	 *          getDescription().equals(description)
	 * @ensures getObjets().containsAll(objets) && objets.containsAll(getObjets())
	 * @ensures getSorties().isEmpty()
	 * @throws NullPointerException
	 *             si la liste des objets pass� est null.
	 */
	public Piece(String description, ArrayList<ObjetZork> objets) {
		if (objets == null) {
			throw new NullPointerException("La liste des objets doit etre different de null.");
		}
		if (description != null) {
			this.description = description;
		} else {
			this.description = "Piece";
		}
		this.objets = new ArrayList<ObjetZork>(objets);
		sorties = new HashMap<String, Piece>();
	}

	/**
	 * Methode, qui nous permet de savoir quelle est la description d'une piece.
	 * 
	 * @return Chaine de caractere, qui represente la description de cette piece.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Methode, qui nous permet de savoir quelles sont des objets Zork qui sont dans
	 * la piece.
	 * 
	 * @return L'instance du type ArrayList<ObjetZork>, qui represente la liste des
	 *         objets Zork qui sont dans la piece.
	 * 
	 * @ensures \result !=null
	 * @pure
	 */
	public ArrayList<ObjetZork> getObjets() {
		return objets;
	}

	/**
	 * Methode, qui nous permet de savoir quelles sont les pieces qui sont en
	 * voisinage par rapport a cette piece.
	 * 
	 * @return L'objet de type Map<String,Piece> qui contient toutes les sorties
	 *         (toutes les pieces) assosi� a une direction (nord, est, sud, ouest).
	 * 
	 * @ensures \result !=null
	 * @pure
	 */
	public Map<String, Piece> getSorties() {
		return sorties;
	}

	/**
	 * Methode, qui permet de modifier description de la piece, par la valeur
	 * specifi�.
	 *
	 * @param description
	 *            Chaine de caracteres, qui represente la nouvelle description de
	 *            cette piece.
	 * 
	 * @requires description!=null
	 * @ensures getDescription().equals(description)
	 * @throws NullPointerException
	 *             si la description pass� est null.
	 */
	public void setDescription(String description) {
		if (description == null) {
			throw new NullPointerException("La description doit �tre different de null.");
		}
		this.description = description;
	}

	/**
	 * Methode qui d�finie les sorties de cette piece. A chaque direction correspond
	 * ou bien une piece ou bien la valeur null signifiant qu'il n'y a pas de sortie
	 * dans cette direction.
	 *
	 * @param nord
	 *            L'instance de la classe Piece, qui represente la sortie nord.
	 * @param est
	 *            L'instance de la classe Piece, qui represente la sortie est.
	 * @param sud
	 *            L'instance de la classe Piece, qui represente la sortie sud.
	 * @param ouest
	 *            L'instance de la classe Piece, qui represente la sortie ouest.
	 * 
	 * @ensures (nord == null) ? pieceSuivant("nord")==null :
	 *          pieceSuivante("nord").equals(nord)
	 * @ensures (est == null) ? pieceSuivant("est")==null :
	 *          pieceSuivante("est").equals(est)
	 * @ensures (sud == null) ? pieceSuivant("sud")==null :
	 *          pieceSuivante("sud").equals(sud)
	 * @ensures (ouest == null) ? pieceSuivant("ouest")==null :
	 *          pieceSuivante("ouest").equals(ouest)
	 * @ensures (nord != null) <==> getSorties.containsKey("nord")
	 * @ensures (est != null) <==> getSorties.containsKey("est")
	 * @ensures (sud != null) <==> getSorties.containsKey("sud")
	 * @ensures (ouest != null) <==> getSorties.containsKey("ouest")
	 */
	public void setSorties(Piece nord, Piece est, Piece sud, Piece ouest) {
		if (nord != null) {
			sorties.put("nord", nord);
		}
		if (est != null) {
			sorties.put("est", est);
		}
		if (sud != null) {
			sorties.put("sud", sud);
		}
		if (ouest != null) {
			sorties.put("ouest", ouest);
		}
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux pieces.
	 *
	 * @param o
	 *            Instance de la class Object, on va comparer notre piece avec cette
	 *            instance.
	 * 
	 * @return Boolean, qui est <br>
	 *         <i>true</i> si les deux pieces ont le meme contenu <br>
	 *         <i>false</i> si les deux pieces ont le contenus differents ou si "o"
	 *         n'est pas une instance de la classe Piece.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof Piece) ==> !\result
	 * @ensures (o instanceof Piece) ==> \result <==>
	 *          (this.getDescription().equals(((Piece) o).getDescription()) &&
	 *          this.getObjets().equals(((Piece) o).getObjets()) &&
	 *          this.getSorties().equals(((Piece) o).getSorties()))
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof Piece))
			return false;
		Piece p = (Piece) o;
		return this.description.equals(p.description) && this.objets.equals(p.objets) && this.sorties.equals(p.sorties);
	}

	/**
	 * Redefinition du methode "hashCode" qui returne une entier, qui represente
	 * hash code de cette piece, tel que si deux pieces sont �gaux, alors ils ont le
	 * meme hash code.
	 * 
	 * @return Entier, qui represent hash code de cette piece.
	 * 
	 * @ensures (\forall Piece p1,p2 ; p1.equals(p2) ; p1.hashCode() ==
	 *          p2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		int hCode = description.hashCode() + objets.hashCode() + sorties.hashCode();
		return hCode;
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cette piece, tel
	 * que si deux piece sont �gaux alors ils ont le m�me chaine.
	 * 
	 * @return Chaine de caractere, qui caracterise cette piece.
	 * 
	 * @ensures \resultat!=null
	 * @ensures (\forall Piece p1,p2 ; p1.equals(p2) ;
	 *          p1.toString().equals(p2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "Piece : " + description;
		s += " Sortie nord : " + sorties.get("nord");
		s += " Sortie est : " + sorties.get("est");
		s += " Sortie sud : " + sorties.get("sud");
		s += " Sortie ouest : " + sorties.get("ouest");
		s += "\n Liste des objets : " + objets.toString();
		return s;
	}

	/**
	 * Methode, qui retourne une description complet d'une piece de la forme: "Vous
	 * etes dans descriptionDeLaPiece Objets : descriptionDesSorties Sorties :
	 * descriptionDesObjets" Cette methode fait appele aux methodes annexes
	 * "descriptionObjets" et "descriptionSorties".
	 *
	 * @return Chaine de caractere, qui represente description affichable de cette
	 *         piece.
	 * 
	 * @ensures \resultat!=null
	 * @ensures (\forall Piece p1,p2 ; p1.equals(p2) ;
	 *          p1.descriptionComplet().equals(p2.descriptionComplet()))
	 * @pure
	 */
	public String descriptionComplet() {
		return "\nVous etes dans " + description + ".\n" + descriptionSorties() + descriptionObjets();
	}

	/**
	 * Methode auxilliaire, qui retourne description de toutes les objets contenus
	 * par cette piece de la forme: "Objets : codeObjet1 (descriptionObjet1)
	 * codeObjet2 (descriptionObjet2) etc" Cette methode est appell� par methode
	 * "descriptionComplet".
	 * 
	 * @return Chaine de caractere, qui represente description affichable des objets
	 *         de cette piece.
	 * 
	 * @ensures \resultat!=null
	 */
	private String descriptionObjets() {
		String resulString = "\nObjets :\n";
		Iterator<ObjetZork> iter = objets.iterator();
		while (iter.hasNext()) {
			ObjetZork oz = iter.next();
			resulString += " " + oz.getCode() + " (" + oz.getDescription() + ")\n";
		}
		return resulString;
	}

	/**
	 * Methode auxilliaire, qui retourne description de toutes les sorties contenus
	 * par cette piece de la forme: "Sorties : directionDeLaSortie1
	 * (descriptionDeLaSortie1) directionDeLaSortie2 (descriptionDeLaSortie2) etc"
	 * Cette methode est appell� par methode "descriptionComplet".
	 * 
	 * @return Chaine de caractere, qui represente description affichable des
	 *         sorties de cette piece.
	 * 
	 * @ensures \resultat!=null
	 */
	private String descriptionSorties() {
		String resulString = "Sorties :\n";
		Set<String> keys = sorties.keySet();
		for (Iterator<String> iter = keys.iterator(); iter.hasNext();) {
			String s = iter.next();
			resulString += " " + s + " (" + sorties.get(s).description + ")\n";
		}
		return resulString;
	}

	/**
	 * Methode, qui retourne la piece atteinte lorsque l'on se d�place a partir de
	 * cette piece dans la direction sp�cifi�e. Si cette piece ne poss�de aucune
	 * sortie dans cette direction, alors ce methode renvoi null.
	 *
	 * @param direction
	 *            Chaine de caractere, qui correspond � la direction dans laquelle
	 *            on souhaite se d�placer
	 * @return Soit L'instance de la classe Piece, qui represente la piece atteinte
	 *         lorsque l'on se d�place dans la direction sp�cifi�e, soit null si
	 *         cette piece ne poss�de aucune sortie dans cette direction.
	 * 
	 * @requires direction!=null
	 * @ensures (getSorties().containsKey(direction)) ?
	 *          \result.equals(getSorties().get(direction)) : \result==null
	 * @throws NullPointerException
	 *             si la direction pass� est null.
	 * @pure
	 */

	public Piece pieceSuivante(String direction) {
		if (direction == null) {
			throw new NullPointerException("La direction doit �tre different de null.");
		}
		return sorties.get(direction);
	}

	/**
	 * Methode, qui permet de savoir est-ce que il existe un objet Zork dans la
	 * piece, qui possede le code specifi�.
	 * 
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'objet � trouver.
	 * @return Boolean, qui est :<br>
	 *         <i>true</i> si un objet pareil existe dans la piece.<br>
	 *         <i>false</i> si un objet pareil n'existe pas dans la piece.
	 * 
	 * @requires code!=null
	 * @ensures \result <==> (\exists ObjetZork oz; getObjets().contains(oz);
	 *          oz.getCode().equals(code))
	 * @throws NullPointerException
	 *             si le code pass� est null.
	 * @pure
	 */
	public boolean contient(String code) {
		if (code == null) {
			throw new NullPointerException("Le code doit �tre different de null.");
		}
		for (ObjetZork oz : objets) {
			if (oz.getCode().equals(code))
				return true;
		}
		return false;
	}

	/**
	 * Methode, qui permet de rechercher un objet Zork dans la piece qui a le m�me
	 * code que le code specifi�. Ce methode retourne le reference vers cet objet,
	 * s'il existe dans la piece, et retourne null sinon.
	 * 
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'objet � trouver.
	 * @return Soit reference vers la premi�re occurence d'objet qui a le code
	 *         pareil, s'il existe dans la piece soit null s'il n'existe pas un
	 *         objet pareil dans la piece.
	 * 
	 * @requires code!=null
	 * @ensures (contient(code)) ? (\exists ObjetZork oz; oz.getCode().equals(code)
	 *          && getObjets().contains(oz) ; \result==oz) : \result==null
	 * @throws NullPointerException
	 *             si le code pass� est null.
	 * @pure
	 */
	public ObjetZork rechercherObjet(String code) {
		if (code == null) {
			throw new NullPointerException("Le code doit �tre different de null.");
		}
		for (ObjetZork oz : objets) {
			if (oz.getCode().equals(code))
				return oz;
		}
		return null;
	}

	/**
	 * Methode, qui permet d'ajouter un objet Zork � la fin de la liste des objets
	 * Zork contenus par cette piece.
	 * 
	 * @param oz
	 *            L'instance de la classe ObjetZork, laquelle on veut ajouter � la
	 *            fin de la liste des objets Zork contenus par la piece.
	 * 
	 * @requires oz!=null
	 * @ensures getObjets().get(objets.size()-1) == oz
	 * @ensures \old(getObjets()).size() == getObjets().size()-1
	 * @ensures (\forall int i; i>=0 && i<getObjets().size()-1; getObjets().get(i)
	 *          == \old(getObjets()).get(i))
	 * @throws NullPointerException
	 *             si l'objet pass� est null
	 */
	public void ajouter(ObjetZork oz) {
		if (oz == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		objets.add(oz);
	}

	/**
	 * Methode, qui nous permet de supprimer un objet Zork dont le code correspond �
	 * la chaine qu'on a pass� en param�tre � partir de la liste des objets Zork. Ce
	 * methode enleve la premi�re occurence de cet objet Zork � partir de la liste
	 * des objets Zork contenus par la piece.
	 * 
	 * @param codeObj
	 *            Chaine de caracteres, qui represente un code d'objet � supprimer.
	 * @return Boolean, qui est: <br>
	 *         <i>true</i> si on a trouv� l'objet Zork pareil et si on a pu enlever
	 *         cet objet Zork de la liste des objets Zork contenus par la piece<br>
	 *         <i>false</i> si la piece ne contient aucun objet avec code pareil, et
	 *         la liste reste inchang�.
	 * 
	 * @requires codeObj!=null
	 * @ensures \result <==> (\exists ObjetZork oz; \old(getObjets()).contains(oz);
	 *          oz.getCode().equals(codeObj))
	 * @ensures (\result ? \old(getObjets()).size() == getObjets().size()+1 :
	 *          \old(getObjets()).size() == getObjets().size())
	 * @throws NullPointerException
	 *             si le code d'objet pass� est null.
	 */
	public boolean retirer(String codeObj) {
		if (codeObj == null) {
			throw new NullPointerException("Le code d'un objet doit etre different de null.");
		}
		for (ObjetZork oz : objets) {
			if (oz.getCode().equals(codeObj)) {
				return objets.remove(oz);
			}
		}
		return false;
	}

	/**
	 * Methode, qui permet d'ajouter la liste des objets de type inconnu, qui herite
	 * ObjetZork, � la fin de la liste des objets Zork contenus par cette piece. Ce
	 * methode est appel� pour executer la commande 'jeterTous'.
	 * 
	 * @param objets
	 *            La liste des objets de type inconnu, qui herite ObjetZork,
	 *            laquelle on va ajouter dans la liste des objets Zork contenus par
	 *            cette piece.
	 * 
	 * @requires objets != null
	 * @ensures getObjets().size() == \old(getObjets().size()) + objets.size()
	 * @ensures getObjets().containsAll(objets)
	 * @throws NullPointerException
	 *             si la liste pass� est null
	 */
	public void ajouterTous(ArrayList<? extends ObjetZork> objets) {
		if (objets == null) {
			throw new NullPointerException("La liste des objets passe doit etre different de null.");
		}
		this.objets.addAll(objets);
	}
}
