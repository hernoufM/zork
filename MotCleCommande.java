/**
 * La classe qui d�finit la liste des commandes valides dans le jeu "Zork".
 *
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 *
 * Classe r�pertoriant l'ensemble des mots-cl� utilisables comme commande dans
 * le jeu. Ces commandes sont stocker dans une tableau non modifiable. Cette
 * implementation nous garantie que seules les commandes donn�es par createur de
 * jeu seront valable. La classe MotCleCommande implemente des methodes qui nous
 * permettront de consulter toutes les commandes valides et verifier si une
 * commande donn�e par utilisateur appartient � la liste des commandes valides.
 * Toutes les instance de cette classe sont �gaux. Chaque analyseur syntaxique
 * contient un attribut de cette classe.
 *
 * @author Michael Kolling
 * @author Marc Champesme (pour la traduction francaise)
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 * 
 * @invariants getCommandes().equals({"aller", "quitter",
 *             "aide","acheter","prendre",
 *             "jeter","consulter","retour","objets","jeterOeil",
 *             "jeterTous","repondre"})
 * @invariant (\forall MotCleCommande mcc1,mcc2; mcc1!=null && mcc2!=null;
 *            mcc1.equals(mcc2))
 */

public class MotCleCommande {
	/**
	 * Chaque instance de cette classe doit contenir une tableau static et constant
	 * contenant tous les mots-cl� valides comme commandes.
	 */
	private static final String commandesValides[] = { "aller", "quitter", "aide", "acheter", "prendre", "jeter",
			"consulter", "retour", "objets", "jeterOeil", "jeterTous", "repondre" };

	/**
	 * Creer une instance de la classe MotCleCommande et initialise la liste des
	 * mots-cl� utilisables comme commande.
	 */
	public MotCleCommande() {
	}

	/**
	 * Methode, qui returne toutes les commandes valides pour le jeu "Zork".
	 * 
	 * @return Le tableau des chaines des caracteres, chaque chaine represente une
	 *         commande valide.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public String[] getCommandes() {
		return commandesValides;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux instance de la
	 * classe MotCleCommande.
	 * 
	 * @param o
	 *            Instance de la classe Object, avec laquelle on va comparer notre
	 *            objet.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si "o" est une instance de la classe MotCleCommande<br>
	 *         <i>false</i>, sinon.
	 * 
	 * @requires o!=null
	 * @ensures (o instanceof MotCleCommande) <==> \result
	 * @throws NullPointerException
	 *             si l'objet passe est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof MotCleCommande))
			return false;
		return true;
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code de toutes les
	 * instances de la classe MotCleCommande.
	 * 
	 * @return Entier, qui represent hash code de toutes instances de la classe
	 *         MotCleCommande.
	 * 
	 * @ensures (\forall MotCleCommande mcc1,mcc2 ; mcc1!=null && mcc2!=null;
	 *          mcc1.hashCode() == mcc2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return 4 * commandesValides.hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de toutes les
	 * instances de la classe MotCleCommande.
	 * 
	 * @return Chaine de caracteres, qui represente chaine de toutes instances de la
	 *         classe MotCleCommande.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall MotCleCommande mcc1,mcc2 ; mcc1!=null && mcc2!=null;
	 *          mcc1.toString().equals(mcc2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "" + commandesValides.toString();
		return s;
	}

	/**
	 * Teste si la chaine de caract�res sp�cifi�e est un mot-cl� de commande valide.
	 *
	 * @param aString
	 *            Chaine de caract�res � tester.
	 * @return Boolean qui est:<br>
	 *         <i>true</i> si la chaine donn�e correspond � une commande valide,
	 *         <i>false</i> si la chaine donn�e ne correspond � aucune commande
	 *         valide ou si la chaine donn�e est null.
	 * 
	 * @ensures aString==null ==> !\result
	 * @ensures \result <==> (\exists int i; i>=0 && i<getCommandes().length;
	 *          getCommandes()[i].equals(aString))
	 * @pure
	 */
	public boolean estCommande(String aString) {
		for (int i = 0; i < commandesValides.length; i++) {
			if (commandesValides[i].equals(aString)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Methode, qui permet d'afficher toutes les commandes valides dans le jeu
	 * "Zork".
	 */
	public void afficherToutesLesCommandes() {
		System.out.println("\nToutes les commandes valides : ");
		for (int i = 0; i < commandesValides.length; i++) {
			System.out.print(commandesValides[i] + "  ");
		}
		System.out.println();
	}
}
