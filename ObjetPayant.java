/**
 * La classe qui d�finit les objets payants dans le jeu "Zork".
 * 
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Les objets payants sont parmi les objets Zork particulaires (les autres sont
 * les cl�s et les objet compos�s). Ils ont les m�me caracteristiques et le m�me
 * compertement que l'objet Zork, sauf que le objet payant a un attribut
 * complementaire qui represente le prix de cet objet en euro. Le prix d'objet
 * payant n'est pas modifiable. L'objet payant est toujours transportable. En
 * plus la classe ObjetPayant r�d�finit le methode "consulter".
 * 
 * Dans le jeu objet payant doit �tre acheter pour que le joueur puisse le
 * prendre. L'attribut 'achete' represente le predicat qui dit est ce que le
 * joueur dej� lui achet�. Si il est deja achet� alors ce objet se comporte
 * comme objet Zork sipmle. Mais il peut toujours le consulter (voir la
 * particularit�, voir le prix). L'objet payant peut �tre acheter sauf si le
 * joueur possede suffisament de la monnaie (des pi�ces). Dans le jeu toutes les
 * pi�ces sont equivalents � 1 euro. Donc le joueur peut acheter objet payant si
 * il a au moins autant de pieces que le prix de cet objet. Apr�s l'achat, le
 * nombre des pieces diminue, et le joueur obtient objet achet�. S'il le jete,
 * il doit pas l'acheter � nouveau, il peut le prendre directement.
 *
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 * 
 * @invariant this.getCode()!=null
 * @invariant this.getDescription()!=null
 * @invariant this.getPoids() >= 0
 * @invariant this.isTransportable()
 * @invariant !op1.equals(op2) ==> !op1.getCode().equals(op2.getCode())
 */
public class ObjetPayant extends ObjetZorkParticulaire {
	/**
	 * Chaque objet payant doit posseder une prix, qui determine combien des pieces
	 * le joueur doit avoir pour acheter cet objet.
	 */
	private int prix;

	/**
	 * L'objet payant doit �tre achet� avant que le joueur le permet de transporter.
	 * Ce attribut dit est-ce que le joueur d�ja achet� ce objet.
	 */
	private boolean achete;

	/**
	 * Constructeur, qui creer l'objet payant et initialise ces attributs par les
	 * valeurs specifi�. L'objet est cre� non-achet�.
	 *
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'un objet qu'on veut
	 *            creer.
	 * @param descrpt
	 *            Chaine de caracteres, qui represente petite description d'objet
	 *            qu'on veut creer.
	 * @param poids
	 *            Entier positif, qui represente le poinds en kg d'un objet qu'on
	 *            veut creer.
	 * @param prix
	 *            Entier positif, qui represente le prix en kg d'un objet qu'on veut
	 *            creer.
	 * 
	 * @requires code!=null
	 * @requires descrpt!=null
	 * @requires poids >= 0
	 * @requires prix >= 0
	 * @ensures getCode().equals(code)
	 * @ensures getDescription().equals(descrpt)
	 * @ensures getPoids() == poids
	 * @ensures getPrix() == prix
	 * @ensures !isAchete()
	 * @throws NullPointerException
	 *             si le code et la description d'un objet pass� sont null.
	 * @throws IllegalArgumentException
	 *             si le poids et le prix d'un objet pass� sont strictement
	 *             inferieurs � 0.
	 */
	public ObjetPayant(String code, String descrpt, int poids, int prix) {
		super(code, descrpt, poids, true);
		if (prix < 0) {
			throw new IllegalArgumentException("Le prix doit �tre positive.");
		}
		this.prix = prix;
		achete = false;
	}

	/**
	 * Methode, qui nous permet de savoir quel est le prix d'un objet payant.
	 * 
	 * @return Un entier positif, qui represente le prix de cet objet.
	 * 
	 * @ensures \result >= 0
	 * @pure
	 */
	public int getPrix() {
		return prix;
	}

	/**
	 * Methode, qui permet de savoir est ce que l'objet a �t� d�ja achet� par
	 * joueur.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si l' objet a d�ja �t� achet� par joueur<br>
	 *         <i>false</i>, sinon.
	 * @pure
	 */
	public boolean isAchete() {
		return achete;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux objets payants.
	 * 
	 * @param o
	 *            Instance de la class Object, avec laquelle on va comparer notre
	 *            objet payant.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux objets payants ont le contenu identique <br>
	 *         <i>false</i>, si les deux objets payants ont les contenus differentes
	 *         ou si "o" n'est pas une instance de la classe ObjetPayant.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof ObjetPayant) ==> !\result
	 * @ensures (o instanceof ObjetPayant) ==> \result <==>
	 *          (this.getCode().equals(((ObjetZork) o).getCode()) &&
	 *          this.getDescription().equals(((ObjetZork) o).getDescription()) &&
	 *          this.getPoids() == ((ObjetZork) o).getPoids() && this.getPrix() ==
	 *          ((ObjetPayant) o).getPrix())
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof ObjetPayant))
			return false;
		return (super.equals(o) && prix == ((ObjetPayant) o).prix);
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * objets payants sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de cet objet payant.
	 * 
	 * @ensures (\forall ObjetPayant op1,op2 ; op1.equals(op2) ; op1.hashCode() ==
	 *          op2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return 30 * super.hashCode() + prix;
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cet objet
	 * payant, tel que si et seulement si deux objets payants sont �gaux alors ils
	 * ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente cet objet payant.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall ObjetPayant op1,op2 ; op1.equals(op2) ;
	 *          op1.toString().equals(op2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "ObjetPayant (" + getCode() + ") : " + getDescription();
		s += " poids = " + getPoids();
		s += " prix = " + getPrix();
		s += " transportable";
		return s;
	}

	/**
	 * R�d�finition du methode "consulter" qui nous permet d'executer la commande
	 * 'consulter' sur l'objet payant.
	 * 
	 * @override
	 */
	public void consulter() {
		System.out.println(this.getDescription());
		System.out.println("Le prix de " + this.getCode() + " est " + this.prix + " euro");
	}

	/**
	 * Methode qui permet de changer etat d'un objet payant. Cet objet devient
	 * achet�, c'est � dire que il se comporte comme un objet Zork simple. Ce
	 * methode nous assure que on ne devra plus achet� ce objet car on a d�ja pay�.
	 * 
	 * @ensures isAchete()
	 */
	public void acheter() {
		achete = true;
	}

}
