import java.util.ArrayList;
import java.util.Iterator;

/**
 * La classe qui d�finit les objets compos�s dans le jeu "Zork".
 * 
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Les objets compos�s sont parmi les objets Zork particulaires (les autres sont
 * les cl�s et les objet payants). Ils ont les m�me caracteristiques et le m�me
 * compertement que l'objet Zork, sauf que le objet compos� peut lui m�me
 * contenir les objets Zork. Donc, il a une attribut complementaire qui est la
 * liste des objets Zork (ArrayList<ObjetZork>). Cette liste represente toutes
 * les objets Zork contenus par l'objet compos�. Un objets compos� peut contenir
 * le nombre illimit� des objets Zork. En plus la classe ObjetCompose r�d�finit
 * le methode "consulter". Si le joueur prend un objet compos�, alors il prend
 * aussi toutes les objets Zork lesquels contient cet objet. Pour acceder aux
 * objets Zork qui sont dans l'objet compos� le joueur doit d'abord consulter
 * cet objet (c'est � dire que il doit utiliser la commande 'consulter' suivi du
 * code d'objet compos�). La commande 'consulter' affiche toutes les objets Zork
 * qui sont dans cet objet compos� et apr�s affichage cette commande transfert
 * toutes les objets Zork � partir d'objet compos� dans la piece courante (c'est
 * � dire que commande enleve toutes les objets Zork � partir d'un objet compos�
 * et ajoutes toutes ses objets Zork dans la piece o� se trouve actuellement cet
 * objet). Si l'objet compos� est consult� lors de sa transportation par joueur,
 * alors toutes ses objets Zork seront trasferer dans la liste des objets
 * transport�s par joueur. Si lors d'un transfert le joueur parviens a sa limit�
 * de poids, alors le reste des objets Zork restera dans l'objet compos�.
 * 
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 *
 * @invariant this.getCode()!=null
 * @invariant this.getDescription()!=null
 * @invariant this.getObjets()!=null
 * @invariant this.getPoids() >= 0
 * @invariant !oc1.equals(oc2) && (oc1.isTransportable() || oc2.isTransportable)
 *            ==> !oc1.getCode().equals(oc2.getCode())
 */
public class ObjetCompose extends ObjetZorkParticulaire {
	/**
	 * Chaque objet compos� contient la liste des objet Zork. Cette liste est
	 * impliment�e par l'instance de la classe ArrayList.
	 */
	private ArrayList<ObjetZork> objets;

	/**
	 * Constructeur, qui creer l'objet compos� et initialise ces attributs par les
	 * valeurs specifi� et cr�er la liste des objets Zork.
	 *
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'un objet compos�
	 *            qu'on veut creer.
	 * @param descrpt
	 *            Chaine de caracteres, qui represente petite description d'objet
	 *            compos� qu'on veut creer.
	 * @param poids
	 *            Entier positif, qui represente le poinds en kg d'un objet compos�
	 *            qu'on veut creer.
	 * @param transportable
	 *            Boolean, qui dit pourrait-il joueur � transporter cet objet
	 *            compos�.
	 * 
	 * @requires code!=null
	 * @requires descrpt!=null
	 * @requires poids >= 0
	 * @ensures getCode().equals(code)
	 * @ensures getDescription().equals(descrpt)
	 * @ensures getPoids()==poids
	 * @ensures isTransportable() <==> transportable
	 * @ensures getObjets().isEmpty()
	 * @throws NullPointerException
	 *             si le code et la description d'un objet pass� sont null.
	 * @throws IllegalArgumentException
	 *             si le poids d'un objet pass� est strictement inferieur � 0.
	 */
	public ObjetCompose(String code, String descrpt, int poids, boolean transportable) {
		super(code, descrpt, poids, transportable);
		objets = new ArrayList<ObjetZork>();
	}

	/**
	 * Methode, qui nous permet de savoir quelles sont des objets Zork contenus par
	 * l'objet compos�.
	 * 
	 * @return L'instance du type ArrayList<ObjetZork>, qui represente la liste des
	 *         objets Zork contenus par l'objet compos�.
	 * 
	 * @ensures \result !=null
	 * @pure
	 */
	public ArrayList<ObjetZork> getObjets() {
		return objets;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux objets compos�s.
	 * 
	 * @param o
	 *            Instance de la class Object, avec laquelle on va comparer notre
	 *            objet compos�.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux objets compos�s ont le contenu identique
	 *         <br>
	 *         <i>false</i>, si les deux objets compos�s ont les contenus
	 *         differentes ou si "o" n'est pas une instance de la classe
	 *         ObjetCompose.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof ObjetCompose) ==> !\result
	 * @ensures (o instanceof ObjetCompose) ==> \result <==>
	 *          (this.getCode().equals(((ObjetZork) o).getCode()) &&
	 *          this.getDescription().equals(((ObjetZork) o).getDescription()) &&
	 *          this.isTransportable() == ((ObjetZork) o).isTransportable() &&
	 *          this.getPoids() == ((ObjetZork) o).getPoids() &&
	 *          this.getObjets().equals(((ObjetCompose) o).getObjets()))
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof ObjetCompose))
			return false;
		return (super.equals(o) && objets.equals(((ObjetCompose) o).objets));
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * objets compos�s sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de cet objet compos�.
	 * 
	 * @ensures (\forall ObjetCompose oc1,oc2 ; oc1.equals(oc2) ; oc1.hashCode() ==
	 *          oc2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return 20 * super.hashCode() + objets.hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cet objet
	 * compos�, tel que si et seulement si deux objets compos�s sont �gaux alors ils
	 * ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente cet objet compos�.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall ObjetCompose oc1,oc2 ; oc1.equals(oc2) ;
	 *          oc1.toString().equals(oc2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "ObjetCompose (" + getCode() + ") : " + getDescription();
		s += " poids = " + getPoids();
		s += (isTransportable()) ? " transportable" : " non transportable";
		s += " La liste des objets : " + getObjets().toString();
		return s;
	}

	/**
	 * R�d�finition du methode "consulter" qui nous permet d'executer la commande
	 * 'consulter' sur l'objet compos�.
	 * 
	 * @override
	 */
	public void consulter() {
		System.out.println(this.getDescription());
		if (objets.isEmpty()) // si la liste des objets Zork est vide, alors on affiche qu'il y a rien
								// particulaire.
		{
			System.out.println("Rien particulaire");
		} else // sinon on affiche toutes les objets qui sont dans cette liste de maniere:"code
				// (description)".
		{
			System.out.println("Tous ce que vous avez trouv� dedans:");
			for (Iterator<ObjetZork> iter = objets.iterator(); iter.hasNext();) {
				ObjetZork oz = iter.next();
				System.out.println(oz.getCode() + " (" + oz.getDescription() + ")");
			}
			System.out.println(
					"\nCes objets sont ajout�s dans la piece. Pour prendre un objet tappez 'prendre' suivi du code d'un objet");
		}
	}

	/**
	 * Methode, qui permet d'ajouter un objet Zork � la fin de la liste des objets
	 * Zork contenus par objet compos�.
	 * 
	 * @param oz
	 *            L'instance de la classe ObjetZork, laquelle on veut ajouter � la
	 *            fin de la liste des objets Zork contenus par l'objet compos�.
	 * 
	 * @requires oz!=null
	 * @ensures getObjets().get(objets.size()-1) == oz
	 * @ensures \old(getObjets()).size() == getObjets().size()-1
	 * @ensures (\forall int i; i>=0 && i<getObjets().size()-1; getObjets().get(i)
	 *          == \old(getObjets()).get(i))
	 * @throws NullPointerException
	 *             si l'objet pass� est null
	 */
	public void ajouter(ObjetZork oz) {
		if (oz == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		objets.add(oz);
	}

	/**
	 * Methode, qui nous permet de supprimer un objet Zork dont le code correspond �
	 * la chaine qu'on a pass� en param�tre � partir de la liste des objets Zork.Ce
	 * methode enleve la premi�re occurence de cet objet Zork � partir de la liste
	 * des objets Zork contenus par l'objet compos�.
	 * 
	 * @param codeObj
	 *            Chaine de caracteres, qui represente un code d'objet � supprimer.
	 * @return Boolean, qui est: <br>
	 *         <i>true</i> si on a trouv� l'objet Zork pareil et si on a pu enlever
	 *         cet objet Zork de la liste des objets Zork contenus par objet
	 *         compos�<br>
	 *         <i>false</i> si l'objet compos� ne contient aucun objet avec code
	 *         pareil, et la liste reste inchang�.
	 * 
	 * @requires codeObj!=null
	 * @ensures \result <==> (\exists ObjetZork oz; \old(getObjets()).contains(oz);
	 *          oz.getCode().equals(codeObj))
	 * @ensures (\result ? \old(getObjets()).size() == getObjets().size()+1 :
	 *          \old(getObjets()).size() == getObjets().size())
	 * @throws NullPointerException
	 *             si le code d'objet pass� est null.
	 */
	public boolean retirer(String codeObj) {
		if (codeObj == null) {
			throw new NullPointerException("Le code d'un objet doit �tre different de null.");
		}
		for (ObjetZork oz : objets) {
			if (oz.getCode().equals(codeObj)) {
				return objets.remove(oz);
			}
		}
		return false;
	}
}
