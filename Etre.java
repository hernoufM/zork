import java.util.*;

/**
 * La classe abstraite qui definit differentes �tres.
 * 
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Cette classe reuni les attributs et les methodes communes pour tous les �tres
 * dans jeu (Fantome et Demon). Les �tre peuvent se deplacer d'une piece �
 * autre. Leur deolacement ne depend pas de joueur ce que garanti que joueur ne
 * peut pas savoir dans quelles pieces ce trouvent les �tre. L'algorithme de
 * deplacement d'une piece � autre consiste � atribuer les nombres de 0 � 4 pour
 * chaque direction (1 - se deplacer au nord, 2 - se deplacer au est, 3 - se
 * deplacer au sud, 4 - se deplacer au ouest, 0 ou autre nombre - rester sur
 * place). Et le choix d'une nombre est faite aleotoirement gr�ce � instance de
 * la classe Random. Si il y a pas sortie vers la direction choisie, alors
 * l'�tre reste sur place. A chaque deplacement de joueur correspond la
 * deplacement de tous les �tres dans jeu. Les �tres representes les obstacles
 * en voie de joueur, car chaque fois quand joueur et une �tre se deplacent dans
 * m�me piece - �tre fait les actions suivantes : 1) Il choisi une question dans
 * sa liste des questions. 2) Il pose cette question � joueur 3) Il donne 4
 * options o� 1 est juste et les autres sont fautives. 4) Il attend jusqu'au le
 * joueur lui donne la lettre d'une option qui est juste. (par exemple "c" pour
 * optionC). La commande doit �tre suivante 'repondre nomEtre reponse' (par
 * exemple 'repondre Abbadon d'. 5) Si la reponse donn� par joueur correspond �
 * la reponse r�elle de cette question, alors l'�tre laise joueur partir. Sinon
 * il lui tue et le jeu est terminer. Chaque fois quand joueur se deplace dans
 * la piece o� se trouve l'�tre, il lui posse la question. Plusieurs �tres
 * peuvent posser des questions dans une seul pieces simultanement. Les
 * questions ne se repete pas. Chaque fois quand l'�tre posse la question, si le
 * joueur donne une reponse juste - alors dans ce cas la question est supprim�
 * de la liste des question. Si il ne reste plus des questions � posser, l'�tre
 * laisse joueur partir.
 * 
 * Chaque �tre possede un nom. Le nom d'�tre ne change jamais. Chaque type
 * d'�tre a des connaisance dans une metier quelconque. Par exemple, les
 * fantomes ma�trise la geographie. La ma�trise ne change jamais. L'�tre peur se
 * trouver dans une seul piece � une instante donn�.
 * 
 * Cette classe definit les methodes qui permettent de consulter les attributs
 * et le methode qui implemente algorithme de deplacement d'�tres. La liste des
 * question ainsi que les methodes de demande de question et methode de la
 * verefication de reponse sont defini dans chaque sous-classe(dans Fantome et
 * dans Demon). C'est pour �a cette classe oblige ses sous-classes (classe
 * Fantome et la classe Demon) de r�d�finir les methode "poserQuestion" et
 * "verefierReponse".
 * 
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 *
 * @invariant getNom()!=null
 * @invariant getMaitrise()!=null
 * @invariant getPieceCourante()!=null
 */
public abstract class Etre {
	/**
	 * Chaine de caractere constante, qui represente le nom d'�tre. Le nom ne change
	 * jamais.
	 */
	private final String nom;
	/**
	 * Chaine de caractere constante, qui represente la ma�trise d'�tre. La ma�trise
	 * ne change jamais.
	 */
	private final String maitrise;
	/**
	 * L'instance de la classe Piece, qui correspond � la piece o� se trouve
	 * actuelement �tre. L'�tre se trouve � un moment donn� dans une seul piece (qui
	 * est la piece courante).
	 */
	private Piece pieceCourante;

	/**
	 * Constructeur, qui permet de creer une �tre et initialiser son nom, sa
	 * ma�trise et la piece o� il se trouve.
	 * 
	 * @param nom
	 *            Chaine de caractere, qui represente le nom d'�tre. Doit etre
	 *            different de null.
	 * @param maitrise
	 *            Chaine de caractere, qui represente la ma�trise d'�tre. Doit etre
	 *            different de null.
	 * @param pieceCourante
	 *            Instance de la classe Piece, o� �tre se trouve au debut. Doit etre
	 *            different de null.
	 * 
	 * @requires nom != null
	 * @requires maitrise != null
	 * @requires pieceCourante != null
	 * @ensures getNom().equals(nom)
	 * @ensures getMaitrise.equals(maitrise)
	 * @ensures getPieceCourante.equals(pieceCourante)
	 * @throws NullPointerException
	 *             si le nom ou la ma�trise ou la piece initiale est null.
	 */
	public Etre(String nom, String maitrise, Piece pieceCourante) {
		if (nom == null || maitrise == null || pieceCourante == null) {
			throw new NullPointerException("Le nom, la ma�trise et la piece initial doit etre non null");
		}
		this.nom = nom;
		this.maitrise = maitrise;
		this.pieceCourante = pieceCourante;
	}

	/**
	 * Methode, qui nous permet de savoir quelle est le nom d'�tre.
	 * 
	 * @return Chaine de caractere, qui represente le nom d'�tre.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Methode, qui nous permet de savoir quelle est la ma�trise d'�tre.
	 * 
	 * @return Chaine de caractere, qui represente la ma�trise d'�tre.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public String getMaitrise() {
		return maitrise;
	}

	/**
	 * Methode, qui nous permet de savoir quelle est la piece courante d'�tre.
	 * 
	 * @return Instance de la classe Piece, qui represente la piece courante d'�tre.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public Piece getPieceCourante() {
		return pieceCourante;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux �tre.
	 * 
	 * @param o
	 *            Instance de la classe Object, avec laquelle on va comparer notre
	 *            �tre.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux �tres ont le nom et la ma�trise identique
	 *         <br>
	 *         <i>false</i>, si les deux �tres ont les noms ou les ma�trises
	 *         differentes ou si "o" n'est pas une instance de la classe Etre.
	 * 
	 * @requires o!=null
	 * @ensures (!(o instanceof Etre)) ==> !\result
	 * @ensures (o instanceof Etre) ==> \result <==> (getNom().equals(((Etre)
	 *          o).getNom()) && getMaitrise().equals(((Etre) o).getMaitrise()))
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof Etre)) {
			return false;
		}
		Etre e = (Etre) o;
		return (nom.equals(e.nom) && maitrise.equals(e.maitrise));
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * �tres sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de cet �tre.
	 * 
	 * @ensures (\forall Etre e1,e2 ; e1.equals(e2) ; e1.hashCode() ==
	 *          e2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return nom.hashCode() + 2 * maitrise.hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cet �tre, tel
	 * que si et seulement si deux �tres sont �gaux alors ils ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente cet �tre.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall Etre e1,e2 ; e1.equals(e2) ;
	 *          e1.toString().equals(e2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String str = "Etre nom = " + nom + " maitrise = " + maitrise;
		return str;
	}

	/**
	 * Methode, qui permet au �tre de se deplacer d'une piece � autre. L'algorithme
	 * de deplacement d'une piece � autre consiste � atribuer les nombres de 0 � 4
	 * pour chaque direction (1 - se deplacer au nord, 2 - se deplacer au est, 3 -
	 * se deplacer au sud, 4 - se deplacer au ouest, 0 ou autre nombre - rester sur
	 * place). Et le choix d'une nombre est faite aleotoirement gr�ce � instance de
	 * la classe Random. Si il y a pas sortie vers la direction choisie, alors
	 * l'�tre reste sur place. Ce methode est appel� pour tous les �tres quand le
	 * joueur se deplace.
	 */
	public void deplacerVersAutrePiece() {
		// choix d'une entier aleatoire entre 0 et 5 (exclu).
		Random rand = new Random();
		int numeroDirection = rand.nextInt(5);
		// assosiation d'une entier � une direction
		String direction;
		switch (numeroDirection) {
		case 1: // 1 est assosi� au direction nord
			direction = "nord";
			if (pieceCourante.pieceSuivante(direction) == null) {
				// s'il n'y a pas la sortie au nord on reste sur place
				return;
			}
			break;
		case 2: // 2 est assosi� au direction est
			direction = "est";
			if (pieceCourante.pieceSuivante(direction) == null) {
				// s'il n'y a pas la sortie au est on reste sur place
				return;
			}
			break;
		case 3: // 3 est assosi� au direction sud
			direction = "sud";
			if (pieceCourante.pieceSuivante(direction) == null) {
				// s'il n'y a pas la sortie au sud on reste sur place
				return;
			}
			break;
		case 4: // 4 est assosi� au direction ouest
			direction = "ouest";
			if (pieceCourante.pieceSuivante(direction) == null) {
				// s'il n'y a pas la sortie au ouest on reste sur place
				return;
			}
			break;
		default: // autre chiffre est equivalente � "rester sur place".
			return;
		}
		pieceCourante = pieceCourante.pieceSuivante(direction);
	}

	/**
	 * Methode, qui permet d'afficher l'information concernant l'�tre (nom et
	 * ma�trise) ainsi que que ce que le joueur doit faire, pour que l'�tre lui
	 * laisse partir. Cette methode est appel� par la methode de sous-classes
	 * "poserQuestion".
	 */
	public void afficherMessage() {
		System.out.println("Bonjour, je suis " + nom + ". Je maitrise " + maitrise + ".\n"
				+ "Je vais vous poser une question sur ce sujet. Si votre reponse"
				+ " sera correcte, je vous laiserai partir. Sinon je vous tuerai.\n"
				+ "Donnez juste la lettre (a,b,c ou d) en utilisant la commande 'repondre' suivi de mon nom suivi de reponse (a, b, c ou d). Voici la question:\n");
	}

	/**
	 * Methode abstraite, qui doit �tre r�d�finit dans les sous-classes (classe
	 * Fantome et la classe Demon). Ce methode doit appeler la methode
	 * "afficherMessage" et choisir la question dans la liste des questions d'�tre.
	 * Le choix est fait aleotoirement. Apres il affiche la question et les options.
	 * Le numero de question est retourn�. Si il n'y a plus de question � posser le
	 * methode retourne -1.
	 * 
	 * @return Entier, qui represente le numero de question dans la liste des
	 *         question d'�tre. Doit etre superieur ou egale � -1.
	 * 
	 * @ensures \result >= -1
	 * @pure
	 */
	public abstract int poserQuestion();

	/**
	 * Methode abstraite, qui doit �tre r�d�finit dans les sous-classes (classe
	 * Fantome et la classe Demon). Ce methode permet de comparer la reponse donn�
	 * par utilisateur avec la reponse r�elle de question dont le numero dans la
	 * liste est specifi�. Si les reponses sont identique, alors le methode supprime
	 * cette question � partir de la liste et retourne true, sinon false. Si l'etre
	 * n'a pas posser la question, alors le numero de queston specifi� doit �tre -1
	 * et le retourne sera toujours true.
	 * 
	 * @param numeroQuestion
	 *            Entier, qui represente le numero de question dans la liste des
	 *            question d'�tre. Doit etre superieur ou egale � -1.
	 * @param reponse
	 *            Chaine de caracteres, qui correspond � reponse donn� par joueur de
	 *            la question dont le numero (numero dans la liste des question
	 *            d'�tre) est specifi�. Doit �tre different de null.
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si la reponse donn� par joueur et la reponse r�elle sont
	 *         identique <br>
	 *         <i>false</i>, sinon.
	 * 
	 * @requires numeroQuestion >=-1
	 * @requires reponse != null
	 * @throws IllegalArgumentException
	 *             si le numero de la question pass� est inferieur � -1.
	 * @throws NullPointerException
	 *             si la reponse donn� est null.
	 */
	public abstract boolean verefierReponse(int numeroQuestion, String reponse);
}
