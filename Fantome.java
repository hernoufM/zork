import java.util.*;

/**
 * Classe, qui definit les fantomes dans le jeu "Zork".
 * 
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Les fantomes sont des �tres, �a veut dire que ils ont le comportement comme
 * les autres �tres (Demon). Cette classe herite toutes les comportement de la
 * classe Etre : comme tous les autres �tres ils possedent le nom et il peuvent
 * se deplacer d'une piece � une autre. Toutes les fantomes ma�trisent que le
 * Geographie. C'est � dire que tous les question sont � propos de Geographie.
 * Les fantomes en plus possede la liste des questions, qui est implement� par
 * ArrayList<Question>. Chaque fantome peut poser au plus 16 questions. Les
 * questions sont initialis�es lors de creation du fantome.
 * 
 * Cette classe possede des methode qui permet de consulter la liste des
 * questions. En plus cette classe redefini les methode abstraite de la
 * super-classe (Etre) qui permettent de poser la question ainsi que donner ses
 * options et qui permet de verefier la reponse donn� par joueur et supprimer
 * cette question de la liste (si la reponse est juste).
 * 
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 *
 * @invariant getNom()!=null
 * @invariant getMaitrise.equals("Geographie")
 * @invariant getPieceCourante != null
 * @invariant getQuestions()!=null
 * @invariant getQuestions().size() >= 0 && getQuestions().size() <= 16
 * @invariant getQuestions().isEmpty() <==> poserQuestion() == -1
 * @invariant !getQuestions().isEmpty() <==> poserQuestions() >= 0 &&
 *            poserQuestions() < getQuestions().size()
 * @invariant getQuestions().isEmpty() ==> (\forall String reponse; reponse !=
 *            null ; verefierReponse(-1, reponse))
 * @invariant !getQuestions().isEmpty() ==> (\forall String reponse, int
 *            numeroQuestion; numeroQuestion == poserQuestion() &&
 *            reponse.equals(getQuestions().get(numeroQuestion).getReponse());
 *            verifier(numeroQuestion, reponse))
 * @invariant !getQuestions().isEmpty() ==> (\forall String reponse, int
 *            numeroQuestion; numeroQuestion == poserQuestion() &&
 *            !reponse.equals(getQuestions().get(numeroQuestion).getReponse());
 *            !verifier(numeroQuestion, reponse))
 */
public class Fantome extends Etre {
	/**
	 * ArrayList des Questions, qui represente toutes les questions qui peuvent �tre
	 * pos�es par un fantome.
	 */
	private ArrayList<Question> questionnaire;

	/**
	 * Constructeur, qui fait appele � constructeur de super classe (Etre). Ce
	 * constructeur en plus intitialise et rempli la liste des questions en ajoutant
	 * 16 questions differentes pour fantome.
	 * 
	 * @param nom
	 *            Chaine de caracteres, qui correspend au nom d'un fantome. Doit
	 *            �tre different de null.
	 * @param pieceInitiale
	 *            Instance de la classe Piece, qui correspend � piece initiale d'un
	 *            fantome. Doit �tre differente de null.
	 * 
	 * @requires nom != null
	 * @requires pieceInitiale != null
	 * @ensures getNom().equals(nom)
	 * @ensures getMaitrise().equals("Geographie")
	 * @ensures getPieceCourante().equals(pieceInitiale)
	 * @ensures getQuestions() != null
	 * @ensures getQuestions().size() == 16
	 * @throws NullPointerException
	 *             si le nom ou la piece initiale est null.
	 */
	public Fantome(String nom, Piece pieceInitiale) {
		super(nom, "Geograpie", pieceInitiale);
		questionnaire = new ArrayList<Question>(16);
		// ajout des questions dans la listes
		questionnaire.add(
				new Question("Quel pays a pour capitale Kaboul?", "Pakistan", "Irak", "Afghanistan", "Yemen", "c"));
		questionnaire.add(
				new Question("Quel pays a pour capitale Tirana?", "Nicaragua", "Albanie", "Turquie", "Ghana", "b"));
		questionnaire.add(new Question("Quelle est la capitale de la Norv�ge?", "Oslo", "Helsinki", "Copenhague",
				"Stockholm", "a"));
		questionnaire
				.add(new Question("Quelle est la capitale de l'Ukraine?", "Tallin", "Kiev", "Moscou", "Minsk", "b"));
		questionnaire.add(
				new Question("Quelle est la capitale de l'Ireland?", "Berlin", "Lisbonne", "Londres", "Dublin", "d"));
		questionnaire.add(new Question("Quelle est la capitale de la Hongrie?", "Belgrade", "Sofia", "Budapest",
				"Chisinau", "c"));
		questionnaire.add(
				new Question("Quelle est la capitale de la Pologne?", "Pristina", "Riga", "Berne", "Varsovie", "d"));
		questionnaire.add(
				new Question("Quelle est la capitale de la Croatie?", "Varsovie", "Tirana", "Zagreb", "Belgrade", "c"));
		questionnaire
				.add(new Question("Quelle est la capitale de la Grece?", "Athenes", "Madrid", "Kiev", "Nicosie", "a"));
		questionnaire
				.add(new Question("Quelle est la capitale de la Syrie?", "Damas", "Bacou", "Bagdad", "Tbilissi", "a"));
		questionnaire
				.add(new Question("Quelle est la capitale de l'Iran?", "Caire", "Bagdad", "Teheran", "Beyrout", "c"));
		questionnaire.add(new Question("Quelle est la capitale de l'Arabie saoudite?", "Mecca", "Abu Dhabi", "Riyad",
				"Bagdad", "c"));
		questionnaire.add(
				new Question("Quelle est la capitale de l'Indonesie?", "Manama", "Jakarta", "Riyad", "Teheran", "b"));
		questionnaire.add(
				new Question("Quelle est la capitale du Chili?", "Buenos Aires", "Santiago", "Sucre", "Bogota", "b"));
		questionnaire
				.add(new Question("Quelle est la capitale d'Uruguay?", "Lima", "Quito", "Asuncion", "Montevideo", "d"));
		questionnaire.add(new Question("Quelle est la capitale de la Venezuela?", "Caracas", "Santiago", "Brasilia",
				"Bogota", "a"));
	}

	/**
	 * Methode, qui permet de consulter la liste des questions qui peuvent �tre pos�
	 * par fantome.
	 * 
	 * @return L'instance du type ArrayList<Question>, qui represente la liste des
	 *         questions qui peuvent �tre pos� par fantome.
	 * 
	 * @ensures \result !=null
	 * @ensures \result.size() >= 0 && \result.size <= 16
	 * @pure
	 */
	public ArrayList<Question> getQuestions() {
		return questionnaire;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux fantome.
	 * 
	 * @param o
	 *            Instance de la classe Object, avec laquelle on va comparer notre
	 *            fantome.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux fantome ont le nom identique <br>
	 *         <i>false</i>, si les deux fantomes ont les noms differentes ou si "o"
	 *         n'est pas une instance de la classe Fantome.
	 * 
	 * @requires o!=null
	 * @ensures (!(o instanceof Fantome)) ==> !\result
	 * @ensures (o instanceof Fantome) ==> \result <==> getNom().equals(((Fantome)
	 *          o).getNom())
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof Fantome)) {
			return false;
		}
		return super.equals(o);
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * fantomes sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de ce fantome.
	 * 
	 * @ensures (\forall Fantome f1,f2 ; f1.equals(f2) ; f1.hashCode() ==
	 *          f2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return 2 * super.hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cet fantome, tel
	 * que si et seulement si deux fantomes sont �gaux alors ils ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente ce fantome.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall Fantome f1,f2 ; f1.equals(f2) ;
	 *          f1.toString().equals(f2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String str = "Fantome nom = " + getNom() + " maitrise = " + getMaitrise();
		return str;
	}

	/**
	 * Redefinition du methode "poserQuestion". Ce methode doit appeler la methode
	 * "afficherMessage" et choisir la question dans la liste des questions de
	 * fantome. Le choix est fait aleotoirement, gr�ce � instance de la classe
	 * Random dont methode a retourn� l'entier entre 0 et la taille de la liste.
	 * Apres il affiche la question et les options. Le numero de question est
	 * retourn�. Si il n'y a plus de question � posser le methode retourne -1.
	 * 
	 * @return Entier, qui represente le numero de question dans la liste des
	 *         question du fantome. Doit �tre �gale � -1 si la liste est vide. Si la
	 *         liste n'est pas vide, la valeur retourn� doit �tre entre 0 et la
	 *         taille de la liste.
	 * 
	 * @ensures getQuestions().isEmpty() <==> \result == -1
	 * @ensures !getQuestions().isEmpty() <==> \result >= 0 && \result <
	 *          getQuestions().size()
	 * @override
	 * @pure
	 */
	public int poserQuestion() {
		afficherMessage();
		if (!questionnaire.isEmpty()) {
			// choix d'une entier aleatoir entre 0 et la taille de la liste (exclu).
			Random rand = new Random();
			int numeroQuestion = rand.nextInt(questionnaire.size());

			Question question = questionnaire.get(numeroQuestion);
			// afficher les options
			System.out.println(question.getQuestion());
			System.out.println("a) " + question.getOptionA());
			System.out.println("b) " + question.getOptionB());
			System.out.println("c) " + question.getOptionC());
			System.out.println("d) " + question.getOptionD());
			return numeroQuestion;
		}
		// si il ne reste plus des questions � posser.
		System.out.println("J'ai vous deja posse tous les questions, alors je vous laisse parti.");
		return -1;
	}

	/**
	 * Redefinition du methode "verifierReponse". Ce methode permet de comparer la
	 * reponse donn� par utilisateur avec la reponse r�elle de question dont le
	 * numero dans la liste est specifi�. Le numero doit etre entre 0 et la taille
	 * de la liste. Si les reponses sont identiques, alors le methode supprime cette
	 * question � partir de la liste et retourne true, sinon false. Si le fantome
	 * n'a pas posser la question, alors le numero de queston specifi� doit �tre -1
	 * et le retourne sera toujours true.
	 * 
	 * @param numeroQuestion
	 *            Entier, qui represente le numero de question dans la liste des
	 *            question du fantome. Doit etre entre 0 et la taille de la liste si
	 *            la liste n'est pas vide. Si la liste est vide doit �tre egale �
	 *            -1.
	 * @param reponse
	 *            Chaine de caracteres, qui correspond � reponse donn� par joueur de
	 *            la question dont le numero (numero dans la liste des question de
	 *            fantome) est specifi�. Doit �tre different de null.
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si la reponse donn� par joueur et la reponse r�elle sont
	 *         identiques <br>
	 *         <i>false</i>, sinon.
	 * 
	 * @requires getQuestions().isEmpty() ==> numeroQuestion == -1
	 * @requires !getQuestions().isEmpty() ==> numeroQuestion >= 0 && numeroQuestion
	 *           < getQuestion().size()
	 * @requires reponse != null
	 * @ensures getQuestions().isEmpty() ==> \result
	 * @ensures !getQuestions().isEmpty() ==>
	 *          reponse.equals(getQuestions().get(numeroQuestion).getReponse()) <==>
	 *          \result
	 * @ensures !getQuestions().isEmpty() && \result ==> \old(getQuestions().size())
	 *          == getQuestions().size()+1
	 * @ensures getQuestions().isEmpty() || !\result ==> \old(getQuestions().size())
	 *          == getQuestions().size()
	 * @ensures !getQuestions().isEmpty() && \result ==> (\forall int i; i>=0 && i <
	 *          numeroQuestion;
	 *          getQuestions().get(i).equals(\old(getQuestions().get(i)))) &&
	 *          (\forall int j; j>=numeroQuestion && j < getQuestions().size();
	 *          getQuestions().get(j).equals(\old(getQuestions().get(j+1))))
	 * @ensures !getQuestions().isEmpty() && !\result ==> (\forall int i; i>=0 && i
	 *          < getQuestions().size();
	 *          getQuestions().get(i).equals(\old(getQuestions().get(i))))
	 * @throws IllegalArgumentException
	 *             dans le cas o� la liste est vide et le numero de la question
	 *             pass� est different de -1.
	 * @throws NullPointerException
	 *             si la reponse donn� est null.
	 * @throws IndexOutOfBounds
	 *             dans le cas o� la liste n'est pas vide et le numero de la
	 *             question pass� n'est pas entre 0 et la taille de la liste.
	 * @override
	 */
	public boolean verefierReponse(int numeroQuestion, String reponse) {
		if (reponse == null) {
			throw new NullPointerException("La reponse doit �tre differente de null");
		}
		if (questionnaire.isEmpty() && numeroQuestion != -1) {
			throw new IllegalArgumentException("Si la liste est vide le numero de question doit etre -1");
		}
		if (!questionnaire.isEmpty() && (numeroQuestion < 0 || numeroQuestion >= questionnaire.size())) {
			throw new IndexOutOfBoundsException(
					"Si la liste n'est pas vide le numero de question doit etre entre 0 et la taille de l liste");
		}
		if (numeroQuestion != -1) {
			Question question = questionnaire.get(numeroQuestion);
			if (!question.verifierReponse(reponse)) {
				return false;
			}
			questionnaire.remove(numeroQuestion);
		}
		return true;
	}

}