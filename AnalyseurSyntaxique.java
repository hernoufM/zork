import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * La classe qui d�finit les analyseurs syntaxiques dans le jeu "Zork".
 *
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Un analyseur syntaxique possede un seul attribut, qui est l'instance de la
 * classe MotCLeCommande, qui correspond au tableau de commandes valides. Chaque
 * fois quand l'utilisateur entre la commande dans terminal, le methode, qui est
 * defini dans cette classe, "getCommande()" lit une ligne au terminal et tente
 * de l'interpr�ter comme une commande (c'est � dire que ce methode lit la ligne
 * entr�e par utilisateur et le represente comme l'instance de la classe
 * Commande, dont le premier attribut correspond au premier mot entr�, deuxieme
 * attribut correspond au dexieme mot entr�, et troisieme au troisieme mot
 * entr�). Le premier mot entr� devrait �tre la commande valide. Le deuxieme et
 * troisieme mot pourraient-�tre les mots quelconque (par exemple: le code d'un
 * objet Zork, la direction de la sortie, le nom d'un monstre etc). Le reste des
 * mots entr�s par utilisateur seront ignor�s. Si le premier mot entr� ne
 * correspond � aucune commande valides, alors le methode "getCommande"
 * retournera l'instance de la classe Commande, dont le premier attribut est
 * null. Si l'utilisateur entre que le premier mot qui correspond � commande
 * valide, alors le methode retournera l'instance de la classe Commande, dont le
 * deuxieme et troisieme attributs sont nulls.
 * 
 * L'analyseur syntaxique possede aussi le methode permettant afficher toutes
 * les commandes valides dans jeu "Zork".
 * 
 * Chaque jeu possede un attribut qui est l'instance de cette classe. Toutes les
 * analyseurs syntaxiques sont egaux, parce que ils possedent les m�mes listes
 * des commandes valides.
 *
 * @author Michael Kolling
 * @author Marc Champesme (pour la traduction francaise)
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 * 
 * @invariant getToutesLesCommandes()!=null
 * @invariant (\forall AnalyseurSyntaxique as1,as2; as1!=null && as2!=null ;
 *            as1.equals(as2))
 */

public class AnalyseurSyntaxique {
	/**
	 * Chaque analyseur syntaxique possede la liste des commandes valides dans le
	 * jeu "Zork". Cette liste est implement� comme �tant l'instance de la classe
	 * MotCleCommande.
	 */
	private MotCleCommande toutesLesCommandes;

	/**
	 * Creer une instance de la classe AnalyseurSyntaxique, et initialise sa liste
	 * des commandes valides.
	 * 
	 * @ensures getToutesLesCommandes()!=null
	 */
	public AnalyseurSyntaxique() {
		toutesLesCommandes = new MotCleCommande();
	}

	/**
	 * Methode qui retourne la liste des toutes commandes valides dans le jeu
	 * "Zork".
	 * 
	 * @return Instance de la classe MotCleCommande, qui represente la liste des
	 *         toutes commandes valides.
	 * 
	 * @ensures \resultat!=null
	 * @pure
	 */
	public MotCleCommande getToutesLesCommandes() {
		return toutesLesCommandes;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux analyseurs
	 * syntaxiques.
	 * 
	 * @param o
	 *            Instance de la classe Object, avec laquelle on va comparer notre
	 *            analyseur syntaxique.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si "o" est une instance de la classe Analyseur
	 *         Syntaxique<br>
	 *         <i>false</i>, sinon.
	 * 
	 * @requires o!=null
	 * @ensures (o instanceof AnalyseurSyntaxique) <==> \result
	 * @throws NullPointerException
	 *             si l'objet passe est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof AnalyseurSyntaxique))
			return false;
		return true;
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code des tout
	 * analyseurs syntaxiques.
	 * 
	 * @return Entier, qui represent hash code des tout analyseurs syntaxiques.
	 * 
	 * @ensures (\forall AnalyseurSyntaxique as1,as2 ; as1!=null && as2!=null;
	 *          as1.hashCode() == as2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		return 2 * getToutesLesCommandes().hashCode();
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine des tout analyseurs
	 * syntaxiques.
	 * 
	 * @return Chaine de caracteres, qui represente chaine de tout analyseurs
	 *         syntaxiques.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall AnalyseurSyntaxique as1,as2 ; as1!=null && as2!=null;
	 *          as1.toString().equals(as2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "Commandes valides : ";
		s += getToutesLesCommandes().toString();
		return s;
	}

	/**
	 * Methode qui lit une ligne au terminal et tente de l'interpr�ter comme
	 * constituant une commande compos�e de trois mots. La commande est alors
	 * renvoy�e sous forme d'une instance de la classe Commande. Si le premier mot
	 * entr� ne correspond � aucune commande valide, alors le methode retournera
	 * l'instance de la classe Commande, dont le premier attribut est null. Si
	 * l'utilisateur entre que le premier mot qui correspond � commande valide,
	 * alors le methode retournera l'instance de la classe Commande, dont le
	 * deuxieme et le troisieme attributs sont nulls. Si utilisateur a donn� que
	 * deux mots alors le troisieme attribut est null. Le reste des mots entr�s par
	 * utilisateur seront ignor�s.
	 *
	 * @return L'instance de la classe Commande, qui correspond � interpretation de
	 *         la ligne entr� par utilisatuer dans le terminal.
	 * 
	 * @ensures \result!=null
	 */
	public Commande getCommande() {
		// Declaration des variables locaux pour m�moriser la ligne entr�e par
		// l'utilisateur.
		String ligneEntree = "";
		String mot1;
		String mot2;
		String mot3;

		// Affichage de l'invite de commande.
		System.out.print("\n> ");

		// Stockage de la ligne entr� par utilisateur dans une variable local.
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			ligneEntree = reader.readLine();
		} catch (java.io.IOException exc) {
			System.out.println("Une erreur est survenue pendant la lecture de votre commande: " + exc.getMessage());
		}

		// Decoupage de la ligne entr� par utilisateur en 3 mots
		StringTokenizer tokenizer = new StringTokenizer(ligneEntree);
		if (tokenizer.hasMoreTokens()) // r�cup�ration du permier mot (le mot commande), si il existe.
		{
			mot1 = tokenizer.nextToken();
		} else // s'il n'existe pas
		{
			mot1 = null;
		}
		if (tokenizer.hasMoreTokens()) // r�cup�ration du deuxieme mot, si il existe
		{
			mot2 = tokenizer.nextToken();
		} else // s'il existe pas
		{
			mot2 = null;
		}
		if (tokenizer.hasMoreTokens()) // r�cup�ration du troisieme mot, si il existe
		{
			mot3 = tokenizer.nextToken();
		} else {
			mot3 = null;
		}
		// Note: le reste de la ligne est ignor�.

		// Creation d'instance de type Commande, et initialisation de ses attributs.
		if (toutesLesCommandes.estCommande(mot1)) // si la premier mot entr� par utilisateur correspond a une commande
													// valide
		{
			return new Commande(mot1, mot2, mot3);
		} else // sinon
		{
			return new Commande(null, mot2, mot3);
		}
	}

	/**
	 * Methode, qui permet d'afficher toutes les commandes valides dans le jeu
	 * "Zork".
	 */
	public void afficherToutesLesCommandes() {
		toutesLesCommandes.afficherToutesLesCommandes();
	}
}
