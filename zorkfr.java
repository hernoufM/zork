//
//  zorkfr.java
//  zorkfr
//
//  Created by Marc on 04/03/06.
//  Copyright (c) 2006 __MyCompanyName__. All rights reserved.
//

/**
 * Class qui cr�e, initialise et demarre le jeu. Cette classe contient le
 * methode main.
 */
public class zorkfr {
	/**
	 * Le methode main de jeu "Zork".
	 * 
	 * @param args
	 *            Tableau de chaines des caracteres qui represente les arguments
	 *            donn�es par utilisateur.
	 */
	public static void main(String args[]) {
		Jeu leJeu;

		leJeu = new Jeu(); // initiailisation de jeu (creation des piece, avec des objets, creation
							// d'analyseur syntaxique et creation de joueur)
		leJeu.jouer(); // demarage de jeu
	}
}
