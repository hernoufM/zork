/**
 * La classe qui d�finit les objets dans le jeu "Zork".
 *
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Un "objet Zork" est une objet quelconque, qui se rencontre dans le jeu. Cette
 * objet, en effet, est un objet quelconque, d'un pierre � un placard. Ils
 * existent des objets qui peuvent �tre transport� par joueur, comme par exemple
 * : le cl�, le pierre, le crayon, la monnaire etc. Mais aussi ils existent des
 * objets qui peuvent pas �tre transport�, comme : la voiture, le distributeur
 * automatique etc.
 * 
 * Objet Zork est caracteris� par son poids, sa description, sa transportabilit�
 * et son code. On manipule avec des objets, qu'en utilisant ses codes. Si au
 * moins un objet parmi deux objets differents est transportable, alors ils ont
 * les codes differents. Deux objets non transportable peuvent avoir le m�me
 * code � condition si ils se trouvent dans les differents pieces. Cette r�gle
 * nous permet d'utiliser le code d'un objet comme �tant un identifiant dans une
 * piece. Toutes les attributs d'objet ne sont pas modifiables. Le poids ne peut
 * pas etre negative. Ils existent des objets de poids 0 (c'est � dire que
 * joueur peut transporter un nombre illimit� de ces objets).
 * 
 * Ils existent des objets particulaires. Ils sont de type
 * ObjetZorkParticulaire. Ce type se decompose sur : les objets de type Cle, les
 * objets de type ObjetPayant et les objet de type ObjetCompose. Ces objets ont
 * les m�mes caracteristiques et le m�me comportement que les simples objets.
 * Sauf ils ont quelleques caracteristiques particulaires.
 * 
 * Le joueur peut prendre et jeter les objets transportable. Il peut aussi
 * consulter toutes les objets (rechercher quelque chose dedans, voir la
 * particularit�). Les objets payants sont exceptionnels car le joueur ne peut
 * pas les prendre, il doit d'abord l'acheter.
 * 
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 * 
 * @invariant this.getCode()!=null
 * @invariant this.getDescription()!=null
 * @invariant this.getPoids() >= 0
 * @invariant !oz1.equals(oz2) && (oz1.isTransportable() || oz2.isTransportable)
 *            ==> !oz1.getCode().equals(oz2.getCode())
 */
public class ObjetZork {
	/**
	 * Un objet quelconque contient le code, qui permet identifier cet objet dans
	 * une piece. Le code n'est pas modifiable.
	 */
	private final String code;
	/**
	 * Un objet quelconque contient une petite description, qui donne une ide� sur
	 * cet objet. La description n'est pas modifiable.
	 */
	private final String description;
	/**
	 * Chaque objet a une poind en kilogramme, qui est positive. Le poids n'est pas
	 * modifiable.
	 */
	private final int poids;
	/**
	 * Objet peut-etre transportable par joueur ou non. On peut pas modifier la
	 * transportabilit� d'un objet.
	 */
	private final boolean transportable;

	/**
	 * Constructeur, qui creer l'objet Zork et initialise ces attributs par les
	 * valeurs specifi�.
	 *
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'un objet qu'on veut
	 *            creer.
	 * @param descrpt
	 *            Chaine de caracteres, qui represente petite description d'un objet
	 *            qu'on veut creer.
	 * @param poids
	 *            Entier positif, qui represente le poinds en kg d'un objet qu'on
	 *            veut creer.
	 * @param transportable
	 *            Boolean, qui dit pourrait-il joueur � transporter cet objet.
	 * 
	 * @requires code!=null
	 * @requires descrpt!=null
	 * @requires poids >= 0
	 * @ensures getCode().equals(code)
	 * @ensures getDescription().equals(descrpt)
	 * @ensures getPoids()==poids
	 * @ensures isTransportable() <==> transportable
	 * @throws NullPointerException
	 *             si le code et la description d'un objet pass� sont null.
	 * @throws IllegalArgumentException
	 *             si le poids d'un objet pass� est strictement inferieur � 0.
	 */
	public ObjetZork(String code, String descrpt, int poids, boolean transportable) {
		if (code == null || descrpt == null) {
			throw new NullPointerException("Le nom et la descriptin doivent �tre different de null.");
		}
		if (poids < 0) {
			throw new IllegalArgumentException("Le poids doit �tre positive.");
		}
		this.code = code;
		description = descrpt;
		this.poids = poids;
		this.transportable = transportable;
	}

	/**
	 * Methode, qui nous permet de savoir quel est le code d'un objet.
	 * 
	 * @return Chaine de caracteres, qui represente le code de cet objet.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Methode, qui nous permet de savoir quelle est la description d'un objet.
	 * 
	 * @return Chaine de caractere, qui represente la description de cet objet.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Methode, qui nous permet de savoir quel est le poids d'un objet.
	 * 
	 * @return Un entier positif, qui represente le poids de cet objet.
	 * 
	 * @ensures \result >= 0
	 * @pure
	 */
	public int getPoids() {
		return poids;
	}

	/**
	 * Permet de savoir si un objet est transportable ou non.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i> si l'objet est transportable <br>
	 *         <i>false</i> si l'objet est non transportable.
	 * @pure
	 */
	public boolean isTransportable() {
		return transportable;
	}

	/**
	 * Redefinition du methode "equals" qui permet de comparer deux objets Zork.
	 * 
	 * @param o
	 *            Instance de la class Object, avec laquelle on va comparer notre
	 *            objet.
	 * 
	 * @return Boolean, qui est :<br>
	 *         <i>true</i>, si les deux objets Zork ont le contenu identiques <br>
	 *         <i>false</i>, si les deux objets Zork ont les contenus differentes ou
	 *         si "o" n'est pas une instance de la classe ObjetZork.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof ObjetZork) ==> !\result
	 * @ensures (o instanceof ObjetZork) ==> \result <==>
	 *          (this.getCode().equals(((ObjetZork) o).getCode()) &&
	 *          this.getDescription().equals(((ObjetZork) o).getDescription()) &&
	 *          this.isTransportable() == ((ObjetZork) o).isTransportable() &&
	 *          this.getPoids() == ((ObjetZork) o).getPoids())
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof ObjetZork))
			return false;
		return (this.code.equals(((ObjetZork) o).code) && this.description.equals(((ObjetZork) o).description)
				&& this.transportable == ((ObjetZork) o).transportable && this.poids == ((ObjetZork) o).poids);
	}

	/**
	 * Redefinition du methode "hashCode" qui returne un hash code, tel que si deux
	 * objets sont �gaux, alors ils ont le m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de cet objet Zork.
	 * 
	 * @ensures (\forall ObjetZork oz1,oz2 ; oz1.equals(oz2) ; oz1.hashCode() ==
	 *          oz2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		int hCode = code.hashCode() + description.hashCode() + 2 * poids;
		hCode *= (transportable) ? 1 : -1;
		return hCode;
	}

	/**
	 * Redefinition du methode "toString" qui returne une chaine de cet objet, tel
	 * que si et seulement si deux objet sont �gaux alors ils ont la m�me chaine.
	 * 
	 * @return Chaine de caractere, qui represente cet objet Zork.
	 * 
	 * @ensures \result!=null
	 * @ensures (\forall ObjetZork oz1,oz2 ; oz1.equals(oz2) ;
	 *          oz1.toString().equals(oz2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "ObjetZork (" + code + ") : " + description;
		s += " poids = " + poids;
		s += (transportable) ? " transportable" : " non transportable";
		return s;
	}

	/**
	 * Methode, qui nous permet d'executer la commande 'consulter' sur l'objet Zork
	 * simple. Les objets Zork particulaires r�d�finissent ce methode.
	 */
	public void consulter() {
		System.out.println(this.getDescription());
		System.out.println("Rien particulaire");
	}

}