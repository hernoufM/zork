import java.util.ArrayList;

/**
 * La classe qui d�finit le joueur dans le jeu "Zork".
 *
 * Cette classe fait partie du logiciel Zork, un jeu d'aventure simple en mode
 * texte.
 * 
 * Le joueur represnte le personnage principale dans le jeu "Zork". Pour une
 * seul jeu il y a un seul joueur. � un moment donn�, le joueur peut se trouver
 * dans une seule piece. Le joueur peut se deplacer d'une piece � une autre.
 * Chaque joueur possede un nom qui est demand� au debut de la jeu. Le joueur
 * possede une poids maximal lequel il peut transporter, cette poids depend du
 * sexe de joueur. Pour homme c'est 6 kg, pour fille 3 kg. Le sexe est demand�
 * au debut de la jeu. Le joueur peut transporter que des objets transportables
 * avec lui. Il ne peut pas transporter des objets que dans la mesure o� le
 * poids total des objets transport�s ne d�passe pas le poids maximal autoris�.
 * Le joueur peut prendre les objets � partie de la piece et jeter les objets
 * dans la piece. Le joueur peut communiquer avec des �tres (repondre au
 * question).
 * 
 * Cette classe contient des methodes qui permettent de manipuler des objets
 * (comme "prendre", "jeter","rechercherObjet", "contientCombienDeCle" etc).
 * 
 * @author Hernouf Mohamed (pour l'amelioration de jeu)
 * @version 2.0
 * @since December 2017
 *
 * @invariant this.getNom()!=null
 * @invariant this.getPoidsMax()==3 || this.getPoidsMax()==6
 * @invariant this.getObjets()!=null
 * @invariant this.poidsTotal(this.getObjets())<=getPoidsMax()
 */
public class Joueur {
	/**
	 * Chaine de caractere constante, qui represente le nom du joueur. Le nom ne
	 * change jamais.
	 */
	private final String nom;
	/**
	 * Entier, qui represente le poids maximal de touts les objets, lequel joueur
	 * peut transporter simultanement. Ce propriet� de joueur ne change jamais.
	 */
	private final int poidsMax;
	/**
	 * ArrayList des ObjetsZork, qui represente toutes les objets qui sont
	 * transport�s par joueur.
	 */
	private ArrayList<ObjetZork> objets;

	/**
	 * Constructeur, qui cre� un joueur avec le nom et le capacite de poids maximal.
	 * Le joueur ne possede acun objet. La liste d'objets est initialis�.
	 * 
	 * @param nom
	 *            Chaine de caracteres, qui represente le nom du joueur
	 * 
	 * @param poidsMax
	 *            Entier, qui represente le nombre maximal de la somme des poids des
	 *            objets lequels joueur peut transporter simultanement
	 * 
	 * @requires nom!=null
	 * @requires poidsMax == 3 || poidsMax == 6
	 * @ensures getNom().equals(nom)
	 * @ensures getPoidsMax() == poids
	 * @ensures getObjets().isEmpty()
	 * @throws NullPointerException
	 *             si le nom du joueur pass� est null.
	 * @throws IllegalArgumentException
	 *             si le poids maximal auturise pour le joueur est different de 3 ou
	 *             different de 6.
	 */
	public Joueur(String nom, int poidsMax) {
		if (nom == null) {
			throw new NullPointerException("Le nom doit �tre different de null.");
		}
		if (poidsMax != 3 && poidsMax != 6) {
			throw new IllegalArgumentException("Le poids maximal auturise pour le joueur doit �tre soit 3 soit 6.");
		}
		this.nom = nom;
		this.poidsMax = poidsMax;
		objets = new ArrayList<ObjetZork>();
	}

	/**
	 * Constructeur, qui cre� un joueur avec le nom et le capacite de poids maximal.
	 * Si le poids total des objets lesquelles on veut lui affecter est superieur �
	 * poids maximal lequel peut transporter joueur alors on lui affecte Arrayliste
	 * vide. Sinon, on lui affecte toutes les objets.
	 * 
	 * @param nom
	 *            Chaine de caracteres, qui represente le nom du joueur
	 * 
	 * @param poidsMax
	 *            Entier, qui represente le maximum de la somme des poids des objets
	 *            lequels joueur peut transporter simultanement
	 * 
	 * @param objets
	 *            ArrayList des ObjetsZork, qui represente toutes les objets qu'on
	 *            veut affecter � joueur
	 * 
	 * @requires nom!=null
	 * @requires poidsMax == 3 || poidsMax == 6
	 * @requires objets!=null
	 * @ensures getNom().equals(nom)
	 * @ensures getPoidsMax() == poids
	 * @ensures (poidsTotal(objets) > poidsMax) ? getObjets().isEmpty() :
	 *          (getObjets().containsAll(objets) && objets.containsAll(getObjets()))
	 * @throws NullPointerException
	 *             si le nom ou la liste des objets du joueur pass� sont null.
	 * @throws IllegalArgumentException
	 *             si le poids maximal auturise pour le joueur est different de 3 ou
	 *             different de 6.
	 */
	public Joueur(String nom, int poidsMax, ArrayList<ObjetZork> objets) {
		if (nom == null || objets == null) {
			throw new NullPointerException("Le nom doit �tre different de null.");
		}
		if (poidsMax != 3 && poidsMax != 6) {
			throw new IllegalArgumentException("Le poids maximal auturise pour le joueur doit �tre soit 3 soit 6.");
		}
		this.nom = nom;
		this.poidsMax = poidsMax;
		if (poidsTotal(objets) > poidsMax) {
			this.objets = new ArrayList<ObjetZork>();
		} else {
			this.objets = new ArrayList<ObjetZork>(objets);
		}
	}

	/**
	 * Methode, qui nous permet de savoir quelle est le nom du joueur.
	 * 
	 * @return Chaine de caractere, qui represente le nom du joueur.
	 * 
	 * @ensures \result!=null
	 * @pure
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Methode, qui nous permet de savoir quelle est le poids maximal de touts les
	 * objets, lequel joueur peut transporter simultanement.
	 * 
	 * @return Chaine de caractere, qui represente le poids maximal de touts les
	 *         objets, lesquelles joueur peut transporter.
	 * 
	 * @ensures \result== 6 || \result== 3
	 * @pure
	 */
	public int getPoidsMax() {
		return poidsMax;
	}

	/**
	 * Methode, qui nous permet de savoir quelles sont des objets Zork port�s par
	 * joueur.
	 * 
	 * @return L'instance du type ArrayList<ObjetZork>, qui represente la liste des
	 *         objets Zork qui sont port�s par joueur.
	 * 
	 * @ensures \result !=null
	 * @pure
	 */
	public ArrayList<ObjetZork> getObjets() {
		return objets;
	}

	/**
	 * Redifinition du methode "equals" qui permet de comparer deux joueurs.
	 *
	 * @param o
	 *            Instance de la class Object, on va comparer notre joueur avec
	 *            cette instance.
	 * 
	 * @return Boolean, qui est <br>
	 *         true, si les deux objets ont le meme contenu <br>
	 *         false, si les deux joueur ont les contenus differents ou si "o" n'est
	 *         pas une instance de la classe Joueur.
	 * 
	 * @requires o!=null
	 * @ensures !(o instanceof Joueur) ==> !\resultat
	 * @ensures (o instanceof Joueur) ==> \resultat <==>
	 *          (this.getNom().equals(((Joueur) o).getNom()) &&
	 *          this.getPoidsMax()==((Joueur) o).getPoidsMax() &&
	 *          this.getObjets().equals(((Joueur) o).getObjets()))
	 * @throws NullPointerException
	 *             si l'objet pass� est null.
	 * @override
	 * @pure
	 */
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (!(o instanceof Joueur))
			return false;
		Joueur j = (Joueur) o;
		return (this.nom.equals(j.nom) && this.objets.equals(j.objets) && this.poidsMax == j.poidsMax);
	}

	/**
	 * Redefinition du methode "hashCode" qui retourne une entier, qui represente
	 * hash code de ce joueur, tel que si deux joueurs sont �gaux, alors ils ont le
	 * m�me hash code.
	 * 
	 * @return Entier, qui represent hash code de ce joueur.
	 * @ensures (\forall Joueur j1,j2 ; j1.equals(j2) ; j1.hashCode() ==
	 *          j2.hashCode())
	 * @override
	 * @pure
	 */
	public int hashCode() {
		int hCode = nom.hashCode() + 5 * poidsMax + objets.hashCode();
		return hCode;
	}

	/**
	 * Redefinition du methode "toString" qui retourne une chaine de ce joueur, tel
	 * que si deux joueurs sont �gaux alors ils ont le m�me chaine.
	 * 
	 * @return Chaine de caractere, qui caracterise ce joueur.
	 * 
	 * @ensures \resultat!=null
	 * @ensures (\forall Joueur j1,j2 ; j1.equals(j2) ;
	 *          j1.toString().equals(j2.toString()))
	 * @override
	 * @pure
	 */
	public String toString() {
		String s = "Joueur : " + nom;
		s += " Poids maximal = " + poidsMax;
		s += "\n Liste des objets : " + objets.toString();
		return s;
	}

	/**
	 * Methode, qui permet d'executer la commande 'objets'. Ce methode retourne une
	 * chaine qui contient la descriptin de toutes les objets port� par joueur de la
	 * forme: "Vous possedez ces objets : codeObjet1 (descriptionObjet1) codeObjet2
	 * (descriptionObjet2) etc" Cette chaine est affich� dans le methode
	 * "afficherObjetsDeJoueur" de la classe Jeu.
	 * 
	 * @return Chaine de caractere, qui represente une description affichable de
	 *         touts les objets port� par joueur.
	 * 
	 * @ensures \resultat!=null
	 * @ensures (\forall Joueur j1,j2; j1.equls(j2);
	 *          j1.descriptionObjets().equals(j2.descriptionObjets()))
	 * @pure
	 */
	public String descriptionObjets() {
		String s = "Vous possedez ces objets :\n";
		for (ObjetZork oz : objets) {
			s += oz.getCode() + " (" + oz.getDescription() + ")\n";
		}
		return s;
	}

	/**
	 * Methode, qui permet d'ajouter un objet Zork � la fin de la liste des objets
	 * Zork port�s par ce joueur, si le poids toral de la somme des touts objets
	 * port�s par joueur et d'objet Zork specifi� est inferieur ou �gal � la limite
	 * du poids maximal.
	 * 
	 * @param oz
	 *            L'instance de la classe ObjetZork, laquelle on veut ajouter � la
	 *            fin de la liste des objets Zork port�s par joueur.
	 * 
	 * @return Boolean qui est :<br>
	 *         <i>true</i> si on a pu d'ajouter l'onjet Zork � la fin de la liste
	 *         <br>
	 *         <i>false</i> sinon.
	 * 
	 * @requires oz!=null
	 * @ensures \result <==> (poidsTotal(getObjets())+oz.getPoids() <=
	 *          getPoidsMax())
	 * @ensures \result ==> getObjets().get(objets.size()-1) == oz
	 * @ensures (\result) ? \old(getObjets()).size() == getObjets().size()-1 :
	 *          \old(getObjets()).size() == getObjets().size()
	 * @ensures (\result) ? (\forall int i; i>=0 && i<getObjets().size()-1;
	 *          getObjets().get(i) == \old(getObjets()).get(i)) : (\forall int i;
	 *          i>=0 && i<getObjets().size(); getObjets().get(i) ==
	 *          \old(getObjets()).get(i))
	 * @throws NullPointerException
	 *             si l'objet pass� est null
	 */
	public boolean prendre(ObjetZork oz) {
		if (oz == null) {
			throw new NullPointerException("Objet passe doit etre different de null");
		}
		if (poidsTotal(objets) + oz.getPoids() > poidsMax) {
			System.out.println("Le joueur est tr�s charg�!");
			return false;
		} else {
			objets.add(oz);
			return true;
		}
	}

	/**
	 * Methode, qui nous permet de supprimer un objet Zork dont le code correspond �
	 * la chaine qu'on a pass� en param�tre � partir de la liste des objet Zork. Ce
	 * methode enleve la premi�re occurence de cet objet Zork � partir de la liste
	 * des objets Zork port�s par joueur.
	 * 
	 * @param codeObj
	 *            Chaine de caracteres, qui represente un code d'objet � supprimer.
	 * @return Boolean, qui est: <br>
	 *         <i>true</i> si on a trouv� l'objet Zork pareil et si on a pu enlever
	 *         cet objet Zork de la liste des objets Zork port�s par joueur<br>
	 *         <i>false</i> si la piece ne contient aucun objet avec code pareil, et
	 *         la liste reste inchang�.
	 * 
	 * @requires codeObj!=null
	 * @ensures \result <==> (\exists ObjetZork oz; \old(getObjets()).contains(oz);
	 *          oz.getCode().equals(codeObj))
	 * @ensures (\result ? \old(getObjets()).size() == getObjets().size()+1 :
	 *          \old(getObjets()).size() == getObjets().size())
	 * @throws NullPointerException
	 *             si le code d'objet pass� est null.
	 */
	public boolean jeter(String codeObj) {
		if (codeObj == null) {
			throw new NullPointerException("Le code d'un objet doit etre different de null.");
		}
		for (ObjetZork oz : objets) {
			if (oz.getCode().equals(codeObj)) {
				return objets.remove(oz);
			}
		}
		return false;
	}

	/**
	 * Methode, qui calcul le poids total du touts les objet Zork qui sont dans
	 * ArrayList specifi�.
	 * 
	 * @param list
	 *            ArrayList des objets de type ObjetZork. C'est pour cette ArrayList
	 *            les calcul sont faites.
	 * 
	 * @return Entier, qui represente le poids total de toutes les objets qui se
	 *         trouvent dans l'ArrayList specifi�.
	 * 
	 * @ensures list==null ==> \result==0
	 * @ensures \result >= 0
	 * @pure
	 */
	public int poidsTotal(ArrayList<ObjetZork> list) {
		int somme = 0;
		for (ObjetZork i : list) {
			somme += i.getPoids();
		}
		return somme;
	}

	/**
	 * Methode, qui calcule combien de monnaie (des pi�ces) joueur possede
	 * actuelement. Dans le jeu toutes les pi�ces sont equivalents � 1 euro. Chaque
	 * pi�ce a le code "1euro".
	 * 
	 * @return Entier, qui represente le nombre d'occurences des pi�ces dans la
	 *         liste des objets Zork port�s par joueur.
	 *
	 * @ensures \result >= 0 && \result < getObjets().size()
	 * @pure
	 */
	public int contientCombienArgent() {
		int solde = 0;
		for (ObjetZork oz : objets) {
			if (oz.getCode().equals("1euro"))
				solde++;
		}
		return solde;
	}

	/**
	 * Methode, qui calcule combien des cl�s de la sortie joueur possede
	 * actuelement. Dans le jeu toutes les cl�s de la sortie ont la description "Cle
	 * de la sortie". Le joueur ne peut pas avoir plus que 5 cl�s de la sorties au
	 * m�me temps.
	 * 
	 * @return Entier, qui represente le nombre d'occurences cl�s de la sortie dans
	 *         la liste des objets Zork port�s par joueur.
	 *
	 * @ensures \result >= 0 && \result <= 5
	 * @pure
	 */
	public int contientCombienDeCle() {
		int cpt = 0;
		for (ObjetZork oz : objets) {
			if (oz.getDescription().equals("Cle de la sortie"))
				cpt++;
		}
		return cpt;
	}

	/**
	 * Methode, qui permet de savoir est-ce que il existe un objet Zork transport�
	 * par joueur, qui possede le code specifi�.
	 * 
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'objet � trouver.
	 * @return Boolean, qui est :<br>
	 *         <i>true</i> si un objet pareil est port� par joueur.<br>
	 *         <i>false</i> si un objet pareil n'est pas port� par joueur.
	 * 
	 * @requires code!=null
	 * @ensures \result <==> (\exists ObjetZork oz; getObjets().contains(oz);
	 *          oz.getCode().equals(code))
	 * @throws NullPointerException
	 *             si le code d'objet pass� est null.
	 * @pure
	 */
	public boolean contient(String code) {
		if (code == null) {
			throw new NullPointerException("Le code d'un objet doit etre different de null.");
		}
		for (ObjetZork oz : objets) {
			if (oz.getCode().equals(code))
				return true;
		}
		return false;
	}

	/**
	 * Methode, qui permet de rechercher un objet Zork dans la liste des objets
	 * port�s par joueur qui a le m�me code que le code specifi�. Ce methode
	 * retourne le reference vers cet objet, s'il est port� par joueur, et retourne
	 * null sinon.
	 * 
	 * @param code
	 *            Chaine de caracteres, qui represente le code d'objet � trouver.
	 * @return Soit reference vers la premi�re occurence d'objet qui a le code
	 *         pareil, s'il est port� par joueur soit null si un objet pareil n'est
	 *         pas port� par joueur.
	 * 
	 * @requires code!=null
	 * @ensures (contient(code)) ? (\exists ObjetZork oz; oz.getCode().equals(code)
	 *          && getObjets().contains(oz) ; \result.equals(oz)) : \result==null
	 * @throws NullPointerException
	 *             si le code d'objet pass� est null.
	 * @pure
	 */
	public ObjetZork rechercherObjet(String code) {
		if (code == null) {
			throw new NullPointerException("Le code d'un objet doit etre different de null.");
		}
		for (ObjetZork oz : objets) {
			if (oz.getCode().equals(code))
				return oz;
		}
		return null;
	}

	/**
	 * Methode, qui permet de supprimer (jeter) et retourner tous les objets �
	 * partir de la liste des objets Zork potr� par joueur. Ce methode est appel�
	 * pour executer la commande 'jeterTous'.
	 * 
	 * @return La liste des objets zork, qui est identique � celui de joueur avant
	 *         d'appelle � ce methode.
	 * 
	 * @ensures getObjets.isEmpty()
	 * @ensures \result.containsAll(\old(getObjets()))
	 * @ensures \old(getObjets().containsAll(\result))
	 */
	public ArrayList<ObjetZork> viderSac() {
		ArrayList<ObjetZork> objets = new ArrayList<ObjetZork>(this.objets.size());
		while (!this.objets.isEmpty()) {
			objets.add(this.objets.remove(0));
		}
		return objets;
	}
}